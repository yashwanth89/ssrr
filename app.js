var express = require('express');
var app = express();
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var port = process.env.PORT || 3000;

// Mongoose
require('./models/db');

// Passport
var passport = require('passport');
require('./config/passport');

// Routes
var routes = require('./routes/index');

var homeRouter = require('./routes/home.route');
var teamRouter = require('./routes/team.route');
var profileRouter = require('./routes/profile.route');
var projectsRouter = require('./routes/projects.route');
var newsRouter = require('./routes/news.route');
var users = require('./routes/users.route');
var adminRouter = require('./routes/admin.route');
var publicationsRouter = require('./routes/publications.route');
var joinUsRouter = require('./routes/joinUs.route.js');

app.disable('x-powered-by');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public/assets/img', 'apple-icon.png')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());

app.use('/', routes);

app.use('/home', homeRouter);
app.use('/team', teamRouter);
app.use('/profile', profileRouter);
app.use('/projects', projectsRouter);
app.use('/news', newsRouter);
app.use('/users', users);
app.use('/admin', adminRouter);
app.use('/publications', publicationsRouter);
app.use('/joinUs', joinUsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

// Catch unauthorised errors
app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({"message" : err.name + ": " + err.message});
  }
});

app.listen(port, function(){
  console.log('Listening on port:' + port);
})
module.exports = app;
