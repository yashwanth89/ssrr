var express = require('express');
var router = express.Router();

var passport = require('passport');
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Member = mongoose.model('Member');
var jwt = require('express-jwt');
var mySecret = 'SSRR_Civil';
var auth = jwt({
  secret: mySecret,
  userProperty: 'payload'
});

// Authentication
router.post('/register', function(req, res, next){
  if(req.body.adminToken === 'SSRR_User'){
    var user = new User(req.body);

    user.setPassword(req.body.password);
    user.save(function(err){
      if(err) { return next(err);};

      var token = user.generateJwt();
      res.status(200);
      res.json({
        'token': token
      });
    });
  } else{
    res.status(401).json({
      "message": "unauthorised!!! Please collect right token from ADMIN"
    })
  }

});
router.post('/login', function(req, res){
  passport.authenticate('local-user', function(err, user, info){

    // If Passport throws/catches an error
    if (err) {
      res.status(404).json(err);
      return;
    }

    if(user.adminPermission){
      // If a user is found
      if(user){
        var token = user.generateJwt();
        res.status(200);
        res.json({
          "token" : token
        });
      } else {
        // If user is not found
        res.status(401).json(info);
      }
    } else{
      res.status(401).json({
        "message": "Permission is pending"
      })
    }

  })(req, res);
});

// Profile
router.get('/profile', auth, function(req, res, next){

  // If no user ID exists in the JWT return a 401
  if (!req.payload._id) {
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else if (!req.payload.adminPermission) {
    res.status(401).json({
      "message" : "Permission is still in pending"
    })
  } else {
    // Otherwise continue
    User.findById(req.payload._id)
      .exec(function(err, user) {
        if(err) { return next(err);};

        if(!user){
          res.json({
            "message" : "User not found"
          });
          return;
        }

        Member.findOne({'emailId': req.payload.email}, function(err, member){
          if(err) {return next(err);};

          if(!member){
              var member  = new Member({
                name: req.payload.name,
                emailId: req.payload.email,
                profession: req.payload.profession
              });

              member.save(function(err, member){
                if(err) { return next(err);};
                Member.count({}, function(err, count) {
                    member.hierarchy = count;

                    member.save(function(err, member){
                      if(err) { return next(err);};

                      res.json(member);
                    });
                });
              });

              return;

          }

          res.json(member);

        });

      });
  }
});

// New member routes
router.get('/newMembers', function(req, res, next){
  User.find({"adminPermission" : false}, { hash: 0, salt: 0, checked: 0})//.select('_id adminPermission email name profession')
    .exec(function(err, users){
      if(err) {return next(err);}

      res.json(users);
    });
});
router.put('/acceptMeber/:id', function(req, res, next){
  User.findOneAndUpdate({
    _id: req.params.id
  },{
    $set: { adminPermission: true}
  }, {
    upsert: true
  }, function(err) {
    if(err) { return next(err);}

    res.status(200).json({
      "message": "Authorization successfull"
    })
  });
});
router.delete('/denyMeber/:id', function(req, res, next){
  User.findOneAndRemove({
    _id: req.params.id
  }, function(err){
    if(err) { return next(err);}

    res.status(200).json({
      "message": "Request deleted"
    })
  })
});

module.exports = router;
