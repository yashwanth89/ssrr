var express = require('express');
var router = express.Router();
var multiparty = require('multiparty');
var fs = require('fs');

var mongoose = require('mongoose');
var AboutUs = mongoose.model('AboutUs');
var Carousel = mongoose.model('Carousel');
var Research = mongoose.model('ResearchAreas');
var ContactUs = mongoose.model('ContactUs');

var jwt = require('express-jwt');
var mySecret = 'SSRR_Admin';
var adminAuth = jwt({secret: mySecret, userProperty: 'adminPayload'});

// About us
router.get('/aboutUs', function(req, res, next){
  AboutUs.find({})
    .exec(function(err, contentObj){
      if(err) { return next(err);}

      res.json(contentObj);
    });
});
router.post('/aboutUs', adminAuth, function(req, res, next){
  if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    var aboutUs = new AboutUs(req.body);

    aboutUs.save(function(err, contentObj){
      if(err) { return next(err);}

      res.json(contentObj);
    });
  }
});
router.put('/aboutUs/:id', adminAuth, function(req, res, next){
  if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    AboutUs.findOneAndUpdate({
      _id: req.params.id
    }, {
      $set : req.body
    }, {
      upsert: true,
      new: true
    }, function(err, contentObj){
      if(err) { return next(err);}

      res.json(contentObj);
    });
  }
});

// Carousel
router.get('/carousel', function(req, res, next){
  Carousel.find({})
    .exec(function(err, slides){
      if(err) {return next(err);}

      res.json(slides);
    });
});
router.post('/carousel', adminAuth, function(req, res, next){
  if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    var form = new multiparty.Form();

    form.parse(req, function(err, fields, files){
      var img = files.image[0];
      // req.body.captionTitle = fields.captionTitle[0];
      if(fields.captionText)
        req.body.captionText = fields.captionText[0];
      fs.readFile(img.path, function(err, data){
        var path = './public/uploads/carousel/' + img.originalFilename;
        fs.writeFile(path, data, function(err) {
          if(err) {return next(err);}

          req.body.imageUrl = path.split('/').slice(2).join('/');
          var slide = new Carousel(req.body);

          slide.save(function(err, slide) {
            if(err) {return next(err);}

            res.json(slide);
          });
        });
      });
    });
  }
});
router.put('/carousel/:id', adminAuth, function(req, res, next){
  if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    var form = new multiparty.Form();

    form.parse(req, function(err, fields, files){
      // req.body.captionTitle = fields.captionTitle[0];
      req.body.captionText = fields.captionText[0];
      req.body.imageUrl = fields.imageUrl[0];
      if(files.image){
        var img = files.image[0];
        fs.readFile(img.path, function(err, data){
          var path = './public/uploads/carousel/' + img.originalFilename;
          fs.writeFile(path, data, function(err) {
            if(err) {return next(err);}

            req.body.imageUrl = 'http://'+ req.headers.host + '/' + path.split('/').slice(2).join('/');
            mongooseCarousel(req.params.id, req.body);
          });
        });
      }
      else {
        mongooseCarousel(req.params.id, req.body);
      }
    });

    function mongooseCarousel(id, body){
      Carousel.findOneAndUpdate({
        _id: id
      },{
        $set: { captionText: body.captionText, imageUrl: body.imageUrl}
      },{
        upsert: true,
        new: true
      },function(err, slide){
        if(err) {return next(err);}

        res.json(slide);
      });
    }
  }
});
router.delete('/carousel/:id', adminAuth, function(req, res, next){
  if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    Carousel.findOneAndRemove({
      _id: req.params.id
    }, function(err, slide){
      if(err) {return next(err);}

      res.json(slide);
    })
  }
});

// Research Areas
router.get('/research', function(req, res, next){
  Research.find({})
    .exec(function(err, researchList){
      if(err) {return next(err);}

      res.json(researchList);
    })
});
router.post('/research', adminAuth, function(req, res, next){
  if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    var research = new Research(req.body);

    research.save(function(err, area){
      if(err) {return next(err);}

      res.json(area);
    });
  }
});
router.put('/research/:id', adminAuth, function(req, res, next){
  if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    Research.findOneAndUpdate({
      _id: req.params.id
    }, {
      $set: { area: req.body.area}
    }, {
      upsert: true,
      new: true
    }, function(err, newArea){
      if(err) {return next(err);}

      res.json(newArea);
    });
  }
});
router.delete('/research/:id', adminAuth, function(req, res, next){
  if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    Research.findOneAndRemove({
      _id: req.params.id
    }, function(err, area){
      if(err) {return next(err);}

      res.json(area);
    });
  }
});

// Contact us
router.get('/contactUs', function(req, res, next){
  ContactUs.find({})
    .exec(function(err, contacts){
      if(err) {return next(err);}

      res.json(contacts[0]);
    });
});
router.post('/contactUs', adminAuth, function(req, res, next){
  if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    ContactUs.find({}).remove()
      .exec(function(err){
        var contactUs = new ContactUs(req.body);

        contactUs.save(function(err, contacts){
          if(err) {return next(err);}

          res.json(contacts);
        });
      });

  }
});
router.put('/contactUs/:id', adminAuth, function(req, res, next){
  if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    ContactUs.findOneAndUpdate({
      _id: req.params.id
    }, {
      $set: req.body
    }, {
      upsert: true,
      new: true
    }, function(err, contact){
      if(err) {return next(err);}

      res.json(contact);
    });
  }
});

module.exports = router;
