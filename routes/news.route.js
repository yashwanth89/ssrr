var express = require('express');
var router = express.Router();
var multiparty = require('multiparty');
var fs = require('fs');

var mongoose = require('mongoose');
var Upcoming = mongoose.model('UpcomingEvents');

var jwt = require('express-jwt');
var mySecret = 'SSRR_Admin';
var adminAuth = jwt({secret: mySecret, userProperty: 'adminPayload'});

// Upcoming Events
router.get('/upcoming', function(req, res, next){
	Upcoming.find({})
		.exec(function(err, eventsList){
			if(err){return next(err);}

			res.json(eventsList);
		})
});
router.post('/upcoming', adminAuth, function(req, res, next){
	if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
		var upcoming = new Upcoming(req.body);

		upcoming.save(function(err, uevent){
			if(err){return next(err);}

				res.json(uevent);
		})
	}
})
router.put('/upcoming/:id', adminAuth, function(req, res, next){
	if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
		Upcoming.findOneAndUpdate({
			_id: req.params.id
		},{
			$set: req.body
		},{
			upsert: true,
			new: true
		},function(err, newEvent){
			if(err){return next(err);}
			res.json(newEvent);
		});
	}
});
router.delete('/upcoming/:id', adminAuth, function(req, res, next){
	if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
	  Upcoming.findOneAndRemove({
	    _id: req.params.id
	  }, function(err, uevent){
	    if(err) {return next(err);}

	    res.json(uevent);
	  });
	}
});
router.put('/newsPage/:id/images', adminAuth, function(req, res, next){
	if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
		var form = new multiparty.Form();

		form.parse(req, function(err, fields, files){
			var img = files.image[0];

			if(fields.caption)
      	req.body.caption = fields.caption[0];
      fs.readFile(img.path, function(err, data){
        var path = './public/uploads/news/images/' + img.originalFilename;
        fs.writeFile(path, data, function(err) {
          if(err) {return next(err);}

          req.body.imageUrl = path.split('/').slice(2).join('/');

					Upcoming.findOneAndUpdate({
						_id: req.params.id
					}, {
						$push: { images: req.body }
					}, {
						upsert: true,
						new: true
					}, function(err, newsPage){
						if(err) { return next(err);}

						res.json(newsPage.images);
					});

        });
      });
		});
	}
});
router.put('/newsPage/:id/image', adminAuth, function(req, res, next){
	if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
		Upcoming.findOneAndUpdate({
			_id: req.params.id
		}, {
			$pull: { images: {imageUrl: req.body.url}}
		}, {
			upsert: true,
			new: true
		}, function(err, newsPage){
			if(err) { return next(err);}

			res.json(newsPage.images);
		});
	}
});
router.put('/newsPage/:id/image/:index', adminAuth, function(req, res, next){
	if(!req.adminPayload._id){
		res.status(401).json({
			"message" : "UnauthorizedError: private profile"
		});
	} else {

		var form = new multiparty.Form();

		form.parse(req, function(err, fields, files){
			if(files.image){
				var img = files.image[0];

				if(fields.caption)
	      	req.body.caption = fields.caption[0];
	      fs.readFile(img.path, function(err, data){
	        var path = './public/uploads/news/images/' + img.originalFilename;
	        fs.writeFile(path, data, function(err) {
	          if(err) {return next(err);}

	          req.body.imageUrl = path.split('/').slice(2).join('/');

						var image = {};
						image['images.' + req.params.index] = {
							caption: req.body.caption,
							imageUrl: req.body.imageUrl
						}

						Upcoming.findOneAndUpdate({
							_id: req.params.id
						}, {
							$set: image
						}, {
							upsert: true,
							new: true
						}, function(err, newsPage){
							if(err) { return next(err);}

							res.json(newsPage.images);
						});

	        });
	      });
			} else{

			}
		});
	}
});

router.put('/newsPage/:id/files', adminAuth, function(req, res, next){
	if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
		var form = new multiparty.Form();

		form.parse(req, function(err, fields, files){
			var img = files.file[0];

			req.body.name = img.originalFilename;
      fs.readFile(img.path, function(err, data){
        var path = './public/uploads/news/files/' + img.originalFilename;
        fs.writeFile(path, data, function(err) {
          if(err) {return next(err);}

          req.body.fileUrl = path.split('/').slice(2).join('/');

					Upcoming.findOneAndUpdate({
						_id: req.params.id
					}, {
						$push: { files: req.body }
					}, {
						upsert: true,
						new: true
					}, function(err, newsPage){
						if(err) { return next(err);}

						res.json(newsPage.files);
					});

        });
      });
		});
	}
});
router.put('/newsPage/:id/file', adminAuth, function(req, res, next){
	if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
		Upcoming.findOneAndUpdate({
			_id: req.params.id
		}, {
			$pull: { files: {fileUrl: req.body.url}}
		}, {
			upsert: true,
			new: true
		}, function(err, newsPage){
			if(err) { return next(err);}

			res.json(newsPage.files);
		});
	}
});
router.put('/newsPage/:id/file/:index', adminAuth, function(req, res, next){
	if(!req.adminPayload._id){
		res.status(401).json({
			"message" : "UnauthorizedError: private profile"
		});
	} else {

		var form = new multiparty.Form();

		form.parse(req, function(err, fields, files){
			if(files.file){
				var img = files.file[0];

				req.body.name = img.originalFilename;
	      fs.readFile(img.path, function(err, data){
	        var path = './public/uploads/news/files/' + img.originalFilename;
	        fs.writeFile(path, data, function(err) {
	          if(err) {return next(err);}

	          req.body.fileUrl = path.split('/').slice(2).join('/');

						var file = {};
						file['files.' + req.params.index] = {
							name: req.body.name,
							fileUrl: req.body.fileUrl
						}

						Upcoming.findOneAndUpdate({
							_id: req.params.id
						}, {
							$set: file
						}, {
							upsert: true,
							new: true
						}, function(err, newsPage){
							if(err) { return next(err);}

							res.json(newsPage.files);
						});

	        });
	      });
			} else{

			}
		});
	}
});

module.exports = router;
