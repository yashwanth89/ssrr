var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var JoinUs = mongoose.model('JoinUs');

var jwt = require('express-jwt');
var mySecret = 'SSRR_Admin';
var adminAuth = jwt({secret: mySecret, userProperty: 'adminPayload'});

router.get('/', function(req, res, next){
  JoinUs.find({})
    .exec(function(err, contentObj){
      if(err) { return next(err);}

      res.json(contentObj[0]);
    });
});
router.post('/:id', adminAuth, function(req, res, next){
  if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    JoinUs.findOneAndRemove({
      _id: req.params.id
    }, function(err){
      if(err) {return next(err);}

      var joinUs = new JoinUs(req.body);

      joinUs.save(function(err, contentObj){
        if(err) { return next(err);}

        res.json(contentObj);
      });
    });
  }
});
router.put('/:id', adminAuth, function(req, res, next){
  if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    JoinUs.findOneAndUpdate({
      _id: req.params.id
    }, {
      $set: req.body
    }, {
      upsert: true,
      new: true
    }, function(err, contentObj){
      if(err) { return next(err);}

      res.json(contentObj);
    });
  }
});

module.exports = router;
