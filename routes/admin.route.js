var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var passport = require('passport');
var Admin = mongoose.model('Admin');

var jwt = require('express-jwt');
var mySecret = 'SSRR_Admin';
var adminAuth = jwt({secret: mySecret, userProperty: 'adminPayload'});

router.post('/register', function(req, res, next){
  if(req.body.adminToken === 'SSRR_Admin') {
    var admin = new Admin(req.body);

    admin.setPassword(req.body.password);
    admin.save(function (err){
      if(err) { return next(err);}

      var token = admin.generateJwt();
      res.status(200).json({
        'token': token
      });
    });
  } else {
    res.status(401).json({
      "message": "unauthorised!!! Please use correct token"
    });
  }
});

router.post('/login', function(req, res, next){
  passport.authenticate('local-admin', function(err, admin, info){
    if(err){ return next(err); }

    if(admin){
      return res.json({'token': admin.generateJwt()});
    } else {
      return res.status(401).json(info);
    }
  })(req, res, next);
});

router.get('/admins', function(req, res, next){
  Admin.find({})
    .exec(function(err, admins){
      if(err) {return next(err);}

      res.json(admins);
    });
});

router.delete('/admins/:id', adminAuth, function(req, res, next){
  if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    Admin.findOneAndRemove({
      _id: req.params.id
    }, function(err, admin){
      if(err) {return next(err);}

      res.json(admin);
    });
  }
});

module.exports = router;
