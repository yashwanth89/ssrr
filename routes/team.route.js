var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var Team = mongoose.model('Member');
var Publication = mongoose.model('Publication');
var Projects = mongoose.model('Projects');

var jwt = require('express-jwt');
var mySecret = 'SSRR_Admin';
var adminAuth = jwt({secret: mySecret, userProperty: 'adminPayload'});

router.get('/', function(req, res, next) {
  Team.find({})
    .exec(function(err, team){
      if(err) { return next(err);}

      res.json(team);
    });
});

router.delete('/:id', adminAuth, function(req, res, next){
  if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    Team.findOneAndRemove({
     _id: req.params.id
    }, function(err, member){
     if(err) { return next(err);}

     res.json(member);
    })
  }
});

router.get('/publications/:id', function(req, res, next) {
  Publication.find({ "members": req.params.id}, {members: 0, projects: 0})
    .exec(function(err, publications){
      if(err) {return next(err);}

      res.json(publications);
    });
});

router.get('/projects/:id', function(req, res, next){
  Projects.find({ "members._id": req.params.id}, { title: 1, date: 1})
    .exec(function(err, projects){
      if(err) {return next(err);}

      res.json(projects);
    });
});

router.put('/:id/hierarchy/:value', adminAuth, function(req, res, next){
  if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    Team.findOneAndUpdate({
      _id: req.params.id
    }, {
      $set: { hierarchy: req.params.value}
    }, {
      upsert: true,
      new: true
    }, function(err, member){
      if(err) {return next(err);}

      res.json(member);
    });
  }
});

module.exports = router;
