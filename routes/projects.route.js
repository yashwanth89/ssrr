var express = require('express');
var router = express.Router();
var multiparty = require('multiparty');
var fs = require('fs');

var mongoose = require('mongoose');
var Projects = mongoose.model('Projects');
var Publication = mongoose.model('Publication');
var Team = mongoose.model('Member');


var jwt = require('express-jwt');
var mySecret = 'SSRR_Admin';
var adminAuth = jwt({secret: mySecret, userProperty: 'adminPayload'});

router.get('/', function(req, res, next){
	Projects.find({})
		.exec(function(err, projectsList){
			if(err){return next(err);}

			res.json(projectsList);
		})
});

router.post('/', adminAuth, function(req, res, next){
	if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
		var projects = new Projects(req.body);

		projects.save(function(err, project){
			if(err){return next(err);}

				res.json(project);
		});
	}
});

router.put('/:id', adminAuth, function(req, res, next){
	if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
		Projects.findOneAndUpdate({
			_id: req.params.id
		},{
			$set: req.body
		},{
			upsert: true,
			new: true
		},function(err, newProject){
			if(err){return next(err);}

			res.json(newProject);
		});
	}
});

router.delete('/:id', adminAuth, function(req, res, next){
	if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
	  Projects.findOneAndRemove({
	    _id: req.params.id
	  }, function(err, project){
	    if(err) {return next(err);}

	    res.json(project);
	  });
	}
});

router.get('/publications/:id', function(req, res, next){
	Publication.find({"projects": req.params.id}, {members: 0, projects: 0})
		.exec(function(err, publications){
			if(err) { return next(err);}

			res.json(publications);
		})
});

router.get('/teamMembers', function(req, res, next){
  Team.find({}, { _id: 1, name: 1, displayName: 1 })
    .exec(function(err, team){
      if(err) {return next(err);}

      res.json(team)
    });
});

router.put('/project/:id/slides', adminAuth, function(req, res, next){
	if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
		var form = new multiparty.Form();

		form.parse(req, function(err, fields, files){
			var img = files.image[0];

			if(fields.caption)
      	req.body.caption = fields.caption[0];
      fs.readFile(img.path, function(err, data){
        var path = './public/uploads/projects/' + img.originalFilename;
        fs.writeFile(path, data, function(err) {
          if(err) {return next(err);}

          req.body.imageUrl = path.split('/').slice(2).join('/');

					Projects.findOneAndUpdate({
						_id: req.params.id
					}, {
						$push: { slides: req.body }
					}, {
						upsert: true,
						new: true
					}, function(err, project){
						if(err) { return next(err);}

						res.json(project.slides);
					});

        });
      });
		});
	}
});

router.put('/project/:id/slide', adminAuth, function(req, res, next){
	if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
		Projects.findOneAndUpdate({
			_id: req.params.id
		}, {
			$pull: { slides: {imageUrl: req.body.url}}
		}, {
			upsert: true,
			new: true
		}, function(err, project){
			if(err) { return next(err);}

			res.json(project.slides);
		});
	}
});

router.put('/project/:id/slide/:index', adminAuth, function(req, res, next){
	if(!req.adminPayload._id){
		res.status(401).json({
			"message" : "UnauthorizedError: private profile"
		});
	} else {

		var form = new multiparty.Form();

		form.parse(req, function(err, fields, files){
			if(files.image){
				var img = files.image[0];

				if(fields.caption)
	      	req.body.caption = fields.caption[0];
	      fs.readFile(img.path, function(err, data){
	        var path = './public/uploads/projects/' + img.originalFilename;
	        fs.writeFile(path, data, function(err) {
	          if(err) {return next(err);}

	          req.body.imageUrl = path.split('/').slice(2).join('/');

						var slide = {};
						slide['slides.' + req.params.index] = {
							caption: req.body.caption,
							imageUrl: req.body.imageUrl
						}

						Projects.findOneAndUpdate({
							_id: req.params.id
						}, {
							$set: slide
						}, {
							upsert: true,
							new: true
						}, function(err, project){
							if(err) { return next(err);}

							res.json(project.slides);
						});

	        });
	      });
			} else{

			}

		});

	}
});

module.exports = router;
