var express = require('express');
var router = express.Router();
var multiparty = require('multiparty');
var fs = require('fs');

var mongoose = require('mongoose');
var Member = mongoose.model('Member');
var Projects = mongoose.model('Projects');
var Publication = mongoose.model('Publication');

var jwt = require('express-jwt');
var mySecret = 'SSRR_Civil';
var auth = jwt({
  secret: mySecret,
  userProperty: 'payload'
});

router.put('/CV/:id', auth, function(req, res, next){
  if (!req.payload._id) {
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    var form = new multiparty.Form();

    form.parse(req, function(err, fields, files){
      if(files.CV){
        var cv = files.CV[0];

        fs.readFile(cv.path, function(err, data){
          var path = './public/uploads/CV/' + cv.originalFilename;

          fs.writeFile(path, data, function(err) {
            if(err) {return next(err);}

            var cvPath = 'http://'+ req.headers.host + '/' + path.split('/').slice(2).join('/');

            Member.findOneAndUpdate({
              _id: req.params.id
            }, {
              $set: {cvUrl: cvPath}
            },{
              upsert: true,
              new: true
            }, function(err, member){
              if(err) {return next(err);}

              res.json({
                "cvUrl": member.cvUrl
              });
            });

          });
        });
      }
    });
  }
});

router.put('/DP/:id', auth, function(req, res, next){
  if (!req.payload._id) {
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    var form = new multiparty.Form();

    form.parse(req, function(err, fields, files){
      if(files.DP){
        var dp = files.DP[0];

        fs.readFile(dp.path, function(err, data) {
          var path = './public/uploads/DP/' + dp.originalFilename;

          fs.writeFile(path, data, function(err) {
            if(err) {return next(err);}

            var dpPath = 'http://'+ req.headers.host + '/' + path.split('/').slice(2).join('/');

            Member.findOneAndUpdate({
              _id: req.params.id
            }, {
              $set: {dpUrl: dpPath}
            },{
              upsert: true,
              new: true
            }, function(err, member){
              if(err) {return next(err);}

              res.json({
                "dpUrl": member.dpUrl
              });
            });


          });
        });
      }
    });
  }
});

router.put('/:id', auth, function(req, res, next){
  if (!req.payload._id) {
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    Member.findOneAndUpdate({
      _id: req.params.id
    }, {
      $set: req.body
    }, {
      upsert: true,
      new: true
    }, function(err, user){
      if (err) { return next(err);};

      res.json(user);
    });
  }
});

router.get('/projectsList', function(req, res, next){
  Projects.find({}, { title: 1, date: 1 })
    .exec(function(err, projects){
      if(err) {return next(err);}

      res.json(projects);
    });
});

router.get('/publications/:id', function(req, res, next){
  Publication.find({ "members": req.params.id}, {members: 0, projects: 0})
    .exec(function(err, publications){
      if(err) {return next(err);}

      res.json(publications);
    });
});

router.get('/projects/:id', function(req, res, next){
  Projects.find({ "members._id": req.params.id}, { title: 1, date: 1})
    .exec(function(err, projects){
      if(err) {return next(err);}

      res.json(projects);
    });
});

module.exports = router;
