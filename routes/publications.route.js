var express =  require('express');
var router = express.Router();

var mongoose = require('mongoose');
var Publication = mongoose.model('Publication');
var Team = mongoose.model('Member');
var Projects = mongoose.model('Projects');
var bibtexParse = require('bibtex-parser-js');

var jwt = require('express-jwt');
var mySecret = 'SSRR_Admin';
var adminAuth = jwt({secret: mySecret, userProperty: 'adminPayload'});

router.get('/', function(req, res, next){
  Publication.find({})
    .exec(function(err, publications){
      if(err) { return next(err);}

      res.json(publications)
    });
});
router.post('/', adminAuth, function(req, res, next) {
  if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    var publication = new Publication(req.body);

    publication.save(function(err, publication) {
      if(err) {return next(err);}

      res.json(publication);
    });
  }
});
router.delete('/:id', adminAuth, function(req, res, next) {
  if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    Publication.findOneAndRemove({
      _id: req.params.id
    }, function(err) {
      if(err) { return next(err);}

      res.json({
        "message": "Publications deleted"
      });
    });
  }
});
router.put('/:id', adminAuth, function(req, res, next) {
  if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    Publication.findOneAndUpdate({
      _id: req.params.id
    },{
      $set: req.body
    },{
      upsert: true,
      new: true
    }, function(err, publication) {
      if(err) {return next(err);}

      res.json(publication);
    });
  }
});
router.post('/bibtext', adminAuth, function(req, res, next){
  if(!req.adminPayload._id){
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    var array = req.body;

    for(var i = 0; i < array.length; i++){
      var publication = new Publication(array[i]);

      publication.save(function(err, publication){
        if(err) {return next(err);}

        res.status(200);
      });
    }
  }
  return;
});
router.get('/teamMembers', function(req, res, next){
  Team.find({}, { _id: 1, name: 1 })
    .exec(function(err, team){
      if(err) {return next(err);}

      res.json(team)
    });
});
router.get('/projectsList', function(req, res, next){
  Projects.find({}, {_id: 1, title: 1})
    .exec(function(err, projects){
      if(err) {return next(err);}

      res.json(projects);
    });
});

module.exports = router;
