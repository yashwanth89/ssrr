var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var EventsSchema = new Schema({
  imageURL: {
  	type: String,
  	default : "https://web.saumag.edu/communications/files/2016/04/time-300x300.png"
  },
  title: String,
  date: {
    type: Date,
    default: Date.now()
  },
  displayTime: String,
  description: String,
  videoUrl: String,
  images: [Schema.Types.Mixed],
  files: [Schema.Types.Mixed],
  authors: String,
  links: [Schema.Types.Mixed]
});

mongoose.model('UpcomingEvents', EventsSchema);
