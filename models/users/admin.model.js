var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var crypto = require('crypto');
var jwt = require('jsonwebtoken');

var adminSchema = new Schema({
  email: {
    type: String,
    unique: true,
    required: true
  },
  username: {
    type: String,
    default: 'Admin'
  },
  checked: {
    type: Boolean,
    required: true
  },
  hash: String,
  salt: String
});

adminSchema.methods.setPassword = function(password){
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
};

adminSchema.methods.validPassword = function(password){
  var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
  return this.hash === hash;
};

adminSchema.methods.generateJwt = function(){
  // set expiration to 1 days
  var today = new Date();
  var exp = new Date(today);
  exp.setDate(today.getDate() + 1);
  var mySecret = 'SSRR_Admin';

  return jwt.sign({
    _id: this._id,
    username: this.username,
    exp: parseInt(exp.getTime() / 1000),
  }, mySecret);
};

mongoose.model('Admin', adminSchema);
