var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var crypto = require('crypto');
var jwt = require('jsonwebtoken');

var userSchema = new Schema({
  email: {
    type: String,
    unique: true,
    required: true
  },
  name: {
    firstName: {type: String, required: true},
    middleName: String,
    lastName: {type: String, required: true}
  },
  profession: {
    type: String,
    required: true
  },
  checked: {
    type: Boolean,
    required: true
  },
  adminPermission: {
    type: Boolean,
    required: true,
    default: false
  },
  hash: String,
  salt: String
});

userSchema.methods.setPassword = function(password){
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
};

userSchema.methods.validPassword = function(password) {
  var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
  return this.hash === hash;
};

userSchema.methods.generateJwt = function(){
  // set expiration to 1 days
  var expiry = new Date();
  expiry.setDate(expiry.getDate() + 1);
  var mySecret = 'SSRR_Civil';
  var totalTeam = 0;

  return jwt.sign({
    _id: this._id,
    email: this.email,
    name: this.name,
    profession: this.profession,
    adminPermission: this.adminPermission,
    exp: parseInt(expiry.getTime() / 1000)
  }, mySecret);
};

mongoose.model('User', userSchema);
