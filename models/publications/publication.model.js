var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var publicationSchema = new Schema({
  author: [Schema.Types.Mixed],
  author_short: [Schema.Types.Mixed],
  editor: [Schema.Types.Mixed],
  editor_short: [Schema.Types.Mixed],
  urls: [Schema.Types.Mixed],
  members: [Schema.Types.ObjectId],
  projects: [Schema.Types.ObjectId],
  bibtex: String,
  booktitle: String,
  chapter: String,
  key: String,
  publisher: String,
  title: String,
  type: String,
  year: String,
  month: String,
  doi: String,
  journal: String,
  pages: String,
  volume: String,
  address: String
});

mongoose.model('Publication', publicationSchema);
