var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProjectsSchema = new Schema({
	title: String,
	abstract: String,
	date: {
		from: {
			type: Date,
			default: Date.now
		},
		to: {
			type: Date,
			default: Date.now
		}
	},
	slides: [Schema.Types.Mixed],
	members: [{
		id: Schema.Types.ObjectId,
		name: {},
		displayName: String
	}],
	content: String,
	publications: {},
	videoUrl: String,
	displayDate: String
});


mongoose.model('Projects', ProjectsSchema);
