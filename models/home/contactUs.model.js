var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var contactUsSchema = new Schema({
  address: String,
  phone: String,
  email: String
});

mongoose.model('ContactUs', contactUsSchema);
