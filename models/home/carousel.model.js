var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CarouselSchema = new Schema({
  imageUrl: { type: String, required: true },
  // captionTitle: String,
  captionText: String
});

mongoose.model('Carousel', CarouselSchema);
