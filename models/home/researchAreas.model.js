var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Research = new Schema({
  area: String
});

mongoose.model('ResearchAreas', Research);
