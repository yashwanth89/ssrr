var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var aboutUsSchema = new Schema({
  content: String
});

mongoose.model('AboutUs', aboutUsSchema);
