var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var memberSchema = new Schema({
  name: {
    firstName: String,
    middleName: String,
    lastName: String
  },
  emailId: {
    type: String,
    unique: true
  },
  profession: String,
  cvUrl: String,
  dpUrl: String,
  experience: Number,
  experienceStatus: String,
  hierarchy: {
    type: Number,
    default: 0
  },
  displayName: String,
  department: {
    type: String,
    default: 'Civil Engineering Department'
  },
  contacts: [Schema.Types.Mixed],
  projects: [],
  memberships: [Schema.Types.Mixed],
  publications: {},
  researchInterests: [Schema.Types.Mixed],
  socialLinks: [Schema.Types.Mixed],
  teachingCourses: [Schema.Types.Mixed]
});

mongoose.model('Member', memberSchema);
