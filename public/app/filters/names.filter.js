(function(){
  'use strict';

  angular
    .module('app')
    .filter('namesFormate', namesFormate);

  namesFormate.$inject = [];

  function namesFormate(){
    return function(array){
      var newArray = [];
      for(var i = 0; i<array.length; i++){
        newArray.push(array[i].split(',').join('').split('.').join(''));
      }
      return newArray.join(', ');
    }
  }
})();
