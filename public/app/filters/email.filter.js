(function(){
  'use strict';

  angular
    .module('app')
    .filter('emailFilter', emailFilter);

  emailFilter.$inject = [];

  function emailFilter(){
    return function(email){
      if(email)
        return email.split('@').join('[AT]');
    }
  }
})();
