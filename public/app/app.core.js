(function(){
  'use strict';

  angular
    .module('app.core', [
      'ui.bootstrap',
      'ui.bootstrap.datetimepicker',
			'ui.router',
			'ngMap',
			'ngAnimate',
      // 'ngTouch',
      'ngFileUpload',
      'localytics.directives',
      'youtube-embed',
      'textAngular'
    ]);
})();
