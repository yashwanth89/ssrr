(function() {
	'use strict';

	angular
    .module('app')
    .config(config)
		.run(run);

  config.$inject = ['$stateProvider', '$urlRouterProvider'];
	run.$inject = ['authService', 'profileService'];

  function config($stateProvider, $urlRouterProvider){
    $stateProvider
      .state('home', {
        url: '/home',
        templateUrl: 'app/components/home/home.html',
        resolve: {
					aboutUsPromise: aboutUsPromise,
          carouselPromise: carouselPromise,
          // eventsListPromise: eventsListPromise,
					researchListPromise: researchListPromise,
					contactsPromise: contactsPromise
        }
      })
      .state('team', {
        url: '/team',
        templateUrl: 'app/components/team/team.html',
				controller: 'teamController',
				controllerAs: 'vm',
				resolve: {
					teamPromise: teamPromise,
					wlMembersPromise: wlMembersPromise
				}
      })
			.state('member', {
        url: '/member/:id',
        templateUrl: 'app/components/team/member.html',
				controller: 'memberController',
				controllerAs: 'vm',
				resolve: {
					memberPromise: memberPromise
				}
      })
      .state('projects', {
        url: '/projects',
        templateUrl: 'app/components/projects/projects.html',
				controller: 'ProjectsController',
				controllerAs: 'vm',
        resolve: {
          projectsListPromise: projectsListPromise,
					projectTeamPromise: projectTeamPromise
        }
      })
			.state('project', {
        url: '/project/:id',
        templateUrl: 'app/components/projects/project.html',
				controller: 'ProjectController',
				controllerAs: 'vm',
        resolve: {
          projectPromise: projectPromise,
					projectPublicationsPromise: projectPublicationsPromise
        }
      })
      .state('news', {
        url: '/news',
        templateUrl: 'app/components/news/news.html',
        controller: 'EventsController',
        controllerAs: 'vm',
        resolve: {
          eventsListPromise: eventsListPromise
        }
      })
			.state('newsPage', {
				url: '/news/:id',
				templateUrl: 'app/components/news/newsPage.html',
				controller: 'NewsPageController',
				controllerAs: 'vm',
				resolve: {
          newsPromise: newsPromise
        }
			})
      .state('login', {
        url: '/login',
        templateUrl: 'app/components/users/login.html',
				onEnter: userDetection
      })
      .state('register', {
        url: '/register',
        templateUrl: 'app/components/users/register.html',
				onEnter: userDetection
      })
			.state('profile', {
				url: '/profile',
				templateUrl: 'app/components/profile/profile.html',
				controller: 'profileController',
				controllerAs: 'vm',
				resolve: {
					profilePromise: profilePromise
				},
				onEnter: userAuthentication
			})
			.state('admins', {
				url: '/admins',
				templateUrl: 'app/components/admin/admin.html',
				controller: 'AdminsController',
				controllerAs: 'vm',
				resolve: {
					adminListPromise: adminListPromise
				},
				onEnter: adminAuthentication
			})
			.state('adminRegister', {
        url: '/adminRegister',
        templateUrl: 'app/components/admin/register.html',
				onEnter: adminDetection
      })
			.state('adminLogin', {
        url: '/adminLogin',
        templateUrl: 'app/components/admin/login.html',
				onEnter: adminDetection
      })
			.state('publications', {
        url: '/publications',
        templateUrl: 'app/components/publications/publications.html',
				controller: 'publicationsController',
				controllerAs: 'vm',
				resolve: {
					publicationsPromise: publicationsPromise,
					pubTeamPromise: pubTeamPromise,
					pubProjectsPromise: pubProjectsPromise
				},
				onEnter: adminAuthentication
      })
			.state('joinUs', {
				url: '/joinUs',
				templateUrl: 'app/components/join-us/join-us.html',
				controller: 'joinUsController',
				controllerAs: 'vm',
				resolve: {
					joinUsPromise: joinUsPromise
				}
			});

    $urlRouterProvider.otherwise('home');
  }

	function run(authService, profileService){
		if(authService.isLoggedIn())
			return profileService.getProfile();
	}

  /* Home */
	aboutUsPromise.$inject = ['aboutUsService'];
  carouselPromise.$inject = ['carouselService'];
  researchListPromise.$inject = ['researchService'];
	contactsPromise.$inject = ['contactUsService'];

	/* Team */
	teamPromise.$inject = ['teamService'];
	wlMembersPromise.$inject = ['teamService'];

	/* Member */
	memberPromise.$inject = ['$stateParams', 'teamService'];

  /* Projects */
  projectsListPromise.$inject = ['projectsService'];
	projectTeamPromise.$inject = ['projectsService'];

   /* Project */
  projectPromise.$inject = ['$stateParams','projectsService'];
	projectPublicationsPromise.$inject = ['$stateParams', 'projectsService'];

  /* News */
  eventsListPromise.$inject = ['eventsService'];

	/* News page */
	newsPromise.$inject = ['$stateParams', 'eventsService']

	/* Publications */
	publicationsPromise.$inject = ['publicationsService'];
	pubTeamPromise.$inject = ['publicationsService'];
	pubProjectsPromise.$inject = ['publicationsService'];
	adminAuthentication.$inject = ['$state', 'adminAuthService'];

	/* Join Us */
	joinUsPromise.$inject = ['joinUsService'];

	/* Users */
	userDetection.$inject = ['$state', 'authService'];

	/* Profile */
	profilePromise.$inject = ['profileService'];
	userAuthentication.$inject = ['$state', 'authService'];

	/* Admin */
	adminDetection.$inject = ['$state', 'adminAuthService'];
	adminListPromise.$inject = ['adminsService'];

  /* Home */
	function aboutUsPromise(aboutUsService){
		return aboutUsService.getContent();
	}
  function carouselPromise(carouselService){
    return carouselService.getAll();
  }
  function researchListPromise(researchService){
    return researchService.getAll();
  }
	function contactsPromise(contactUsService){
		return contactUsService.getcontacts();
	}

	/* Team */
	function teamPromise(teamService){
		if(!teamService.teamList[0])
			return teamService.getAll();
	}
	function wlMembersPromise(teamService){
		return teamService.getAllWl();
	}

	/* Member */
	function memberPromise($stateParams, teamService){
		return teamService.getMember($stateParams.id)
	}

  /* Projects */
  function projectsListPromise(projectsService){
    return projectsService.getAll();
  }
	function projectTeamPromise(projectsService){
		return projectsService.teamList();
	}

   /* Project */
  function projectPromise($stateParams, projectsService){
   	return projectsService.getProject($stateParams.id);
  }
	function projectPublicationsPromise($stateParams, projectsService){
		return projectsService.getProjectPublicaitons($stateParams.id);
	}

  /* News */
  function eventsListPromise(eventsService){
		if(!eventsService.eventsList[0])
    	return eventsService.getAll();
  }

	/* News page */
	function newsPromise($stateParams, eventsService){
		return eventsService.getNews($stateParams.id);
	}

	/* Publciations */
	function publicationsPromise(publicationsService){
		return publicationsService.getAll();
	}
	function pubTeamPromise(publicationsService){
		return publicationsService.getTeamMembers();
	}
	function pubProjectsPromise(publicationsService){
		return publicationsService.getProjectsList();
	}
	function adminAuthentication($state, adminAuthService){
		if(!adminAuthService.isLoggedIn())
			$state.go('adminLogin');
	}

	/* Join us */
	function joinUsPromise(joinUsService){
		return joinUsService.getContent();
	}

	/* Users */
	function userDetection($state, authService){
		if(authService.isLoggedIn())
			$state.go('profile');
	}

	/* Profile */
	function profilePromise(profileService){
		if(Object.keys(profileService.user).length == 0)
			return profileService.getProfile();
	}
	function userAuthentication($state, authService){
		if(!authService.isLoggedIn())
			$state.go('login');
	}


	/* Admin */
	function adminDetection($state, adminAuthService){
		if(adminAuthService.isLoggedIn())
			$state.go('home');
	}
	function adminListPromise(adminsService){
		return adminsService.getAll();
	}

})();
