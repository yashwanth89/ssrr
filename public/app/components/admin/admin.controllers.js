(function(){
  'use strict';

  angular
    .module('app')
    .controller('AdminAuthController', AdminAuthController)
    .controller('AdminsController', AdminsController);

    AdminAuthController.$inject = ['adminAuthService'];
    AdminsController.$inject = ['adminsService'];

    function AdminAuthController(adminAuthService){
      var vm = this;

      vm.admin = {};
      vm.emailFormat = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
      vm.passwordPattern = /^(?=.*[A-Za-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
      vm.register = register;
      vm.logIn = logIn;

      function register(isValid){
        if(isValid){
          adminAuthService.register(vm.registration);
        }
      }

      function logIn(isValid){
        if(isValid){
          adminAuthService.logIn(vm.login);
        }
      }
    }

    function AdminsController(adminsService){
      var vm = this;

      vm.adminList = adminsService.adminList;
      vm.removeAdmin = removeAdmin;

      function removeAdmin(id, index){
        adminsService.deleteAdmin(id, index);
      }
    }
})();
