(function(){
  'use strict';

  angular
    .module('app')
    .factory('adminAuthService', adminAuthService)
    .factory('adminsService', adminsService);

  adminAuthService.$inject = ['$http', '$window', '$state'];
  adminsService.$inject = ['$http', 'adminAuthService'];

  function adminAuthService($http, $window, $state){
    var o = {
      saveToken: saveToken,
      getToken: getToken,
      isLoggedIn: isLoggedIn,
      currentAdmin: currentAdmin,
      register: register,
      logIn: logIn,
      logOut: logOut
    }

    return o;

    function saveToken(token){
      $window.localStorage['SSRR-Admin-token'] = token;
    }

    function getToken(){
      return $window.localStorage['SSRR-Admin-token'];
    }

    function isLoggedIn(){
      var token = o.getToken();

      if(token){
        var adminPayload = JSON.parse($window.atob(token.split('.')[1]));

        return adminPayload.exp > Date.now() / 1000;
      } else {
        return false;
      }
    }

    function currentAdmin() {
      if(isLoggedIn()){
        var token = o.getToken();
        var adminPayload = JSON.parse($window.atob(token.split('.')[1]));

        return adminPayload.username;
      }
    }

    function register(admin) {
      return $http.post('/admin/register', admin)
        .then(registerComplete)
        .catch(registerFailed);

      function registerComplete(response){
        o.saveToken(response.data.token);
        $state.go('home');
      }

      function registerFailed(error){
        console.log(error);
      }
    }

    function logIn(admin){
      return $http.post('/admin/login', admin)
        .then(loginComplete)
        .catch(loginFailed);

      function loginComplete(response){
        o.saveToken(response.data.token);
        $state.go('home');
      }

      function loginFailed(error){

      }
    }

    function logOut(){
      $window.localStorage.removeItem('SSRR-Admin-token');
      $state.go('home');
    }
  }

  function adminsService($http, adminAuthService){
    var adminList = [];
    var o = {
      adminList: adminList,
      getAll: getAll,
      deleteAdmin: deleteAdmin
    }

    return o;

    function getAll(){
      return $http.get('/admin/admins')
        .then(getComplete)
        .catch(getFailed);

      function getComplete(response){
        angular.copy(response.data, o.adminList);
      }

      function getFailed(error){

      }
    }

    function deleteAdmin(id, index){
      return $http.delete('/admin/admins/' + id, {
          data: { _id: id },
          headers: { Authorization: 'Bearer ' + adminAuthService.getToken()}
        })
        .then(deleteComplete)
        .catch(deleteFailed);

      function deleteComplete(response){
        o.adminList.splice(index, 1);
      }

      function deleteFailed(error){

      }
    }
  }
})();
