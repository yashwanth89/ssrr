(function(){
	'use strict';

	angular
		.module('app')
		.controller('EventsController', EventsController)
		.controller('NewsPageController', NewsPageController);

		EventsController.$inject = ['eventsService', 'adminAuthService'];
		NewsPageController.$inject = ['eventsService'];

		function EventsController(eventsService, adminAuthService) {
	    var vm = this;

	    vm.upcomingEventsList = eventsService.upcomingEventsList;
	    vm.pastEventsList = eventsService.pastEventsList;

	    vm.type = 'Save';
	    vm.newIndex = null;
	    vm.addOrSaveEvent = addOrSaveEvent;
	    vm.updateEvent = updateEvent;
	    vm.removeEvent = removeEvent;

			// Images
			vm.image = {};
			vm.imageIndex = null;
			vm.imageType = 'Save Image';
			vm.removeImage = removeImage;
			vm.updateImage = updateImage;
			vm.addImage = addImage;

			// Files
			vm.file = {};
			vm.fileIndex = null;
			vm.fileType = 'Save File';
			vm.removeFile = removeFile;
			vm.updateFile = updateFile;
			vm.addFile = addFile;

			// links
			vm.newEvent = {};
			vm.newEvent.links = [];
			vm.linksType = 'Add +';
			vm.addlink = addlink;
      vm.linkIndex = null;
      vm.removeLink = removeLink;
      vm.updateLink = updateLink;

			vm.isAdminLoggedIn = adminAuthService.isLoggedIn;

	    function addOrSaveEvent(){
	    	if(vm.type == 'Update'){
	    		eventsService.update(vm.eventType, vm.newEvent, vm.newIndex)
	    		vm.type = 'Save'
	    	}
	    	else{
	    		vm.newEvent.date = vm.newEvent.date.getTime();
	    		eventsService.create(vm.newEvent);
	    	}

	    	vm.newEvent= {};
	    }

	    function updateEvent(type, uevent, index){
	    	vm.newEvent = uevent;
	    	vm.newIndex = index;
	    	vm.type = 'Update';
	    	vm.eventType = type;
	    }

	    function removeEvent(id, type, index){
	    	eventsService.deleteEvent(id, type, index);
	    }

			// Images
			function addImage(){
				if(vm.imageType == 'Update Image'){
					eventsService.updateImage(vm.newEvent._id, vm.imageIndex, vm.image)
					vm.imageType = 'Save Image';
				} else
					eventsService.addImage(vm.newEvent._id, vm.image);

				vm.newEvent.images = eventsService.eventsObj[vm.newEvent._id].images;
				vm.image = {};
			}

			function removeImage(url){
				eventsService.deleteImage(vm.newEvent._id, url);
				vm.newEvent.images = eventsService.eventsObj[vm.newEvent._id].images;
			}

			function updateImage(index, image){
				vm.image = image;
				vm.imageIndex = index;
				vm.imageType = 'Update Image';
			}

			// Files
			function addFile(){
				if(vm.fileType == 'Update File'){
					eventsService.updateFile(vm.newEvent._id, vm.fileIndex, vm.file)
					vm.fileType = 'Save File';
				} else
					eventsService.addFile(vm.newEvent._id, vm.file);

				vm.newEvent.files = eventsService.eventsObj[vm.newEvent._id].files;
				vm.file = {};
			}

			function removeFile(url){
				eventsService.deleteFile(vm.newEvent._id, url);
				vm.newEvent.files = eventsService.eventsObj[vm.newEvent._id].files;
			}

			function updateFile(index, file){
				vm.file = file;
				vm.fileIndex = index;
				vm.fileType = 'Update File';
			}

			//links
			function addlink(){
        if(vm.linksType === 'Update'){
          vm.newEvent.links[vm.membershipIndex] = vm.newEvent.link;
          vm.linksType = 'Add +';
        } else
          vm.newEvent.links.push(vm.newEvent.link);

        vm.newEvent.link = {};
      }

      function removeLink(index){
        vm.newEvent.links.splice(index, 1);
      }

      function updateLink(link, index){
        vm.newEvent.link = link;
        vm.linksType = 'Update';
        vm.linkIndex = index;
      }

	}

	function NewsPageController(eventsService){
		var vm = this;
		vm.news = eventsService.news;

		// Images
		vm.showAlbum = showAlbum;
		vm.presentIndex = 0;
		vm.isActive = isActive;
		vm.showPrev = showPrev;
		vm.showNext = showNext;
		vm.showPhoto = showPhoto;
		vm.galleryActive = false;
		vm.closeGallery = closeGallery;

		function showAlbum(index) {
			vm.galleryActive = true;
			vm.presentIndex = index;
			// vm.isActive();
		}

		function isActive(index) {
			return vm.presentIndex === index;
		}

		function showPrev() {
			vm.presentIndex = (vm.presentIndex > 0) ? --vm.presentIndex : vm.news.images.length - 1;
		}

		function showNext() {
			vm.presentIndex = (vm.presentIndex < vm.news.images.length - 1) ? ++vm.presentIndex : 0;
		}

		function showPhoto(index) {
			vm.presentIndex = index;
		}

		function closeGallery(){
			vm.galleryActive = false;
		}

	}

})();
