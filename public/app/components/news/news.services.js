(function(){
	'use strict';

	angular
		.module('app')
		.factory('eventsService', eventsService)

		eventsService.$inject = ['$http', 'adminAuthService', 'Upload'];


		function eventsService($http, adminAuthService, Upload){
	    var eventsList = [];
			var eventsObj = {};
			var news = {};
			var upcomingEventsList = [];
			var pastEventsList = [];
	    var o = {
	      eventsList: eventsList,
				eventsObj: eventsObj,
				news: news,
				upcomingEventsList: upcomingEventsList,
				pastEventsList: pastEventsList,
	      getAll: getAll,
				getNews: getNews,
	      create: create,
	      update: update,
	      deleteEvent: deleteEvent,
				addImage: addImage,
				deleteImage: deleteImage,
				updateImage: updateImage,
				addFile: addFile,
				deleteFile: deleteFile,
				updateFile: updateFile
	    }

	    return o;

	    function getAll(){
	      return $http.get('/news/upcoming')
	        .then(getComplete)
	        .catch(getFailed);

	        function getComplete(response){
	        	angular.copy(response.data, o.eventsList);

						angular.forEach(o.eventsList, function(value){
							o.eventsObj[value._id] = value;
							if(new Date(value.date).getTime() >  new Date().getTime())
								o.upcomingEventsList.push(value);
							else
								o.pastEventsList.push(value);
						});
	        }
	        function getFailed(error){

	        }
	    }
	    function create(uevent) {
	    	return $http.post('/news/upcoming', uevent, {headers: { Authorization: 'Bearer ' + adminAuthService.getToken()}})
	    	  .then(createComplete)
	    	  .catch(createFailed);

	    	  function createComplete(response){
						if(new Date(response.data.date).getTime() >  new Date().getTime())
							o.upcomingEventsList.push(response.data);
						else
							o.pastEventsList.push(response.data);
	    	  }
	    	  function createFailed(error){

	    	  }
	    }
	    function update(type, uevent, index){
	    	return $http.put('/news/upcoming/' + uevent._id, uevent, {headers: { Authorization: 'Bearer ' + adminAuthService.getToken()}})
	    	  .then(updateComplete)
	    	  .catch(updateFailed);

	    	  function updateComplete(response){
						if(new Date(response.data.date).getTime() >  new Date().getTime()){
							if(type == 'upcoming'){
								o.upcomingEventsList[index] = response.data;
							}
							else {
								o.pastEventsList.splice(index,1);
								o.upcomingEventsList.push(response.data);

							}
						} else {
							if(type == 'past')
								o.pastEventsList[index] = response.data;
							else {
								o.upcomingEventsList.splice(index,1);
								o.pastEventsList.push(response.data);
							}
						}

	    	  }
	    	  function updateFailed(error){

	    		}
	 		}

		function deleteEvent(id, type, index){
			return $http.delete('/news/upcoming/' + id, {
				data: {_id:id},
				headers: { Authorization: 'Bearer ' + adminAuthService.getToken()}
			})
			.then(deleteComplete)
			.catch(deleteFailed);

			function deleteComplete(response){
				o.eventsList.splice(index,1)
				if(type == 'upcoming')
					o.upcomingEventsList.splice(index,1);
				else
					o.pastEventsList.splice(index,1);
			}
			function deleteFailed(error){

			}
		}

		function getNews(id) {
			if(o.eventsList.length === 0){
				o.getAll()
					.then(getNewsComplete)
					.catch(getNewsFailed);

					function getNewsComplete(response){
						angular.copy(o.eventsObj[id], o.news);
					}

					function getNewsFailed(error) {

					}
			}
			else{
				angular.copy(o.eventsObj[id], o.news);
			}
		}

		function addImage(id, slide){
			return Upload.upload({
					url: '/news/newsPage/'+ id + '/images',
					method: 'PUT',
					data: slide,
					headers: {Authorization: 'Bearer ' + adminAuthService.getToken()}
				})
				.progress(addProgress)
				.then(addComplete)
				.catch(addFailed);

			function addProgress(event){
				console.log(Math.floor((event.loaded / event.total)*100)+ "%");
			}

			function addComplete(response){
				o.eventsObj[id].images = response.data;
			}

			function addFailed(error){

			}
		}

		function deleteImage(id, url){
			return $http.put('/news/newsPage/' + id + '/image', { url: url}, {
					headers: {Authorization: 'Bearer ' + adminAuthService.getToken()}
				})
				.then(deleteComplete)
				.catch(deleteFailed);

			function deleteComplete(response){
				o.eventsObj[id].images = response.data;
			}

			function deleteFailed(error){

			}
		}

		function updateImage(id, index, image){
			return Upload.upload({
					url: '/news/newsPage/' + id + '/image/' + index,
					method: 'PUT',
					data: image,
					headers: {Authorization: 'Bearer ' + adminAuthService.getToken()}
				})
				.progress(addProgress)
				.then(addComplete)
				.catch(addFailed);

			function addProgress(event){
				console.log(Math.floor((event.loaded / event.total)*100)+ "%");
			}

			function addComplete(response){
				o.eventsObj[id].images = response.data;
			}

			function addFailed(error){

			}

		}

		function addFile(id, file){
			return Upload.upload({
					url: '/news/newsPage/'+ id + '/files',
					method: 'PUT',
					data: file,
					headers: {Authorization: 'Bearer ' + adminAuthService.getToken()}
				})
				.progress(addProgress)
				.then(addComplete)
				.catch(addFailed);

			function addProgress(event){
				console.log(Math.floor((event.loaded / event.total)*100)+ "%");
			}

			function addComplete(response){
				o.eventsObj[id].files = response.data;
			}

			function addFailed(error){

			}
		}

		function deleteFile(id, url){
			return $http.put('/news/newsPage/' + id + '/file', { url: url}, {
					headers: {Authorization: 'Bearer ' + adminAuthService.getToken()}
				})
				.then(deleteComplete)
				.catch(deleteFailed);

			function deleteComplete(response){
				o.eventsObj[id].files = response.data;
			}

			function deleteFailed(error){

			}
		}

		function updateFile(id, index, file){
			return Upload.upload({
					url: '/news/newsPage/' + id + '/file/' + index,
					method: 'PUT',
					data: file,
					headers: {Authorization: 'Bearer ' + adminAuthService.getToken()}
				})
				.progress(addProgress)
				.then(addComplete)
				.catch(addFailed);

			function addProgress(event){
				console.log(Math.floor((event.loaded / event.total)*100)+ "%");
			}

			function addComplete(response){
				o.eventsObj[id].files = response.data;
			}

			function addFailed(error){

			}

		}

	}

})();
