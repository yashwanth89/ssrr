(function(){
	'use strict';

	angular
		.module('app')
		.controller('ProjectsController', ProjectsController)
		.controller('ProjectController', ProjectController);

		ProjectsController.$inject = ['projectsService', '$state', 'adminAuthService'];
		ProjectController.$inject = ['projectsService'];

		function ProjectsController(projectsService, $state, adminAuthService){
			var vm = this;

			vm.projects = projectsService.projectsList;
			vm.teamList = projectsService.teamMembersList;
			vm.teamListObj = projectsService.teamMembersListObj;

			vm.orderProject = '-date.to';

			vm.type = 'Save';
			vm.newIndex = null;
			vm.addOrSaveProject = addOrSaveProject;
			vm.updateMode = false;
			vm.updateProject = updateProject;
			vm.removeProject = removeProject;
			vm.projectPage = projectPage;
			vm.members = [];

			vm.project = {}

			/* Contents */
			vm.project.contents = [];
			vm.addContent = addContent;

			/* Slides */
			vm.slide = {};
			vm.slideIndex = null;
			vm.slideType = 'Save Slide';
			vm.removeSlide = removeSlide;
			vm.updateSlide = updateSlide;
			vm.addSlide = addSlide;

			vm.isAdminLoggedIn = adminAuthService.isLoggedIn;

			function addOrSaveProject(){
				if(vm.type == 'Update'){
					vm.project.members = [];
					for(var i = 0; i < vm.members.length; i++){
						vm.project.members[i] = vm.teamListObj[vm.members[i]];
					}
					projectsService.update(vm.project, vm.newIndex);
					vm.type = 'Save'
				}
				else{
					projectsService.create(vm.project);
				}

				vm.project = {};
			}

			function updateProject(project, index){
				vm.members = [];
				vm.project = project;
				for(var i = 0; i < vm.project.members.length; i++){
					vm.members.push(vm.project.members[i]._id);
				}
				vm.newIndex = index;
				vm.type = 'Update';
				vm.updateMode = true;
			}

			function removeProject(id, index){
				projectsService.deleteProject(id, index);
			}

			function projectPage(id){
				$state.go('project', {id: id});
			}

			function addContent(){
				// if(!vm.project.contents[0])
				// 	vm.project.contents = [];
				vm.project.contents.push(vm.project.content);
				vm.project.content = {};
			}

			// Slides
			function addSlide(){
				if(vm.slideType == 'Update Slide'){
					projectsService.updateSlide(vm.project._id, vm.slideIndex, vm.slide)
					vm.slideType = 'Save Slide';
				} else
					projectsService.addSlide(vm.project._id, vm.slide);

				vm.project.slides = projectsService.projectObj[vm.project._id].slides;
				vm.slide = {};
			}

			function removeSlide(url){
				projectsService.deleteSlide(vm.project._id, url);
				vm.project.slides = projectsService.projectObj[vm.project._id].slides;
			}

			function updateSlide(index, slide){
				vm.slide = slide;
				vm.slideIndex = index;
				vm.slideType = 'Update Slide';
			}

		}


		function ProjectController(projectsService){
			var vm = this;
			vm.project = projectsService.project;

			vm.tabContent = true;
		}
})();
