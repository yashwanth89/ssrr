(function () {
	'use strict';

	angular
		.module('app')
		.factory('projectsService', projectsService)

		projectsService.$inject = ['$http', 'adminAuthService', 'Upload'];

		function projectsService($http, adminAuthService, Upload){
			var projectsList = [];
			var projectObj = {};
			var project = {};
			var teamMembersList = [];
			var teamMembersListObj = {};
			var o = {
				projectsList : projectsList,
				projectObj : projectObj,
				project : project,
				getAll : getAll,
				getProject : getProject,
				create: create,
				update : update,
				deleteProject : deleteProject,
				getProjectPublicaitons: getProjectPublicaitons,
				teamMembersList: teamMembersList,
				teamList: teamList,
				addSlide: addSlide,
				deleteSlide: deleteSlide,
				updateSlide: updateSlide,
				teamMembersListObj: teamMembersListObj
			}
			return o;

			function getAll(){
				return $http.get('/projects')
					.then(getComplete)
					.catch(getFailed)

					function getComplete(response){
						angular.copy(response.data, o.projectsList);
						angular.forEach(o.projectsList, function(value){
							o.projectObj[value._id] = value;
						});
					}

					function getFailed(error){

					}
			}

			function create(project){
				return $http.post('/projects', project, {headers: {Authorization: 'Bearer ' + adminAuthService.getToken()}})
					.then(createComplete)
					.catch(createFailed)

					function createComplete(response){
						o.projectsList.push(response.data)
					}
					function createFailed(error){

					}
			}

			function update(project, index){
				return $http.put('/projects/' + project._id, project, {headers: {Authorization: 'Bearer ' + adminAuthService.getToken()}})
					.then(updateComplete)
					.catch(updateFailed)

					function updateComplete(response){
						o.projectsList[index] = response.data;
					}
					function updateFailed(error){

					}

			}

			function deleteProject(id,index){
				return $http.delete('/projects/' + id, {
						data: {_id:id},
						headers: {Authorization: 'Bearer ' + adminAuthService.getToken()}
					})
					.then(deleteComplete)
					.catch(deleteFailed)

					function deleteComplete(response){
						o.projectsList.splice(index,1)
					}
					function deleteFailed(error){

					}
			}

			function getProject(id) {
				if(o.projectsList.length === 0){
					o.getAll()
						.then(getProjectComplete)
						.catch(getProjectFailed);

						function getProjectComplete(response){
							angular.copy(o.projectObj[id], o.project);
						}

						function getProjectFailed(error) {

						}
				}
				else{
					angular.copy(o.projectObj[id], o.project);
				}
			}

				function getProjectPublicaitons(id){
					return $http.get('/projects/publications/' + id)
						.then(getComplete)
						.catch(getFailed)

						function getComplete(response){
							o.project.publications = {
			          bookChapters: [],
			          journalPapers: [],
			          conferencePapers: []
			        };
			        for(var i = 0; i < response.data.length; i++){
			          if(response.data[i].type === 'incollection')
			            o.project.publications.bookChapters.push(response.data[i]);
			          else if(response.data[i].type === 'article')
			            o.project.publications.journalPapers.push(response.data[i]);
			          else if(response.data[i].type === 'inproceedings')
			            o.project.publications.conferencePapers.push(response.data[i]);
			        }
						}

						function getFailed(error){

						}
				}

				function teamList() {
					return $http.get('/projects/teamMembers')
						.then(getComplete)
						.catch(getFailed);

					function getComplete(response){
						angular.copy(response.data, o.teamMembersList);
						for(var i = 0; i < o.teamMembersList.length; i++){
							o.teamMembersListObj[o.teamMembersList[i]._id] = o.teamMembersList[i];
						}
						// console.log(o.teamMembersList);
					}

					function getFailed(error){

					}
				}

				function addSlide(id, slide){
					return Upload.upload({
							url: '/projects/project/'+ id + '/slides',
							method: 'PUT',
							data: slide,
							headers: {Authorization: 'Bearer ' + adminAuthService.getToken()}
						})
						.progress(addProgress)
						.then(addComplete)
						.catch(addFailed);

					function addProgress(event){
						console.log(Math.floor((event.loaded / event.total)*100)+ "%");
					}

					function addComplete(response){
						o.projectObj[id].slides = response.data;
					}

					function addFailed(error){

					}
				}

				function deleteSlide(id, url){
					return $http.put('/projects/project/' + id + '/slide', { url: url}, {
							headers: {Authorization: 'Bearer ' + adminAuthService.getToken()}
						})
						.then(deleteComplete)
						.catch(deleteFailed);

					function deleteComplete(response){
						o.projectObj[id].slides = response.data;
					}

					function deleteFailed(error){

					}
				}

				function updateSlide(id, index, slide){
					return Upload.upload({
							url: '/projects/project/' + id + '/slide/' + index,
							method: 'PUT',
							data: slide,
							headers: {Authorization: 'Bearer ' + adminAuthService.getToken()}
						})
						.progress(addProgress)
						.then(addComplete)
						.catch(addFailed);

					function addProgress(event){
						console.log(Math.floor((event.loaded / event.total)*100)+ "%");
					}

					function addComplete(response){
						o.projectObj[id].slides = response.data;
					}

					function addFailed(error){

					}

				}

			}
})();
