(function(){
  'use strict';

  angular
    .module('app')
    .controller('authController', authController);

    authController.$inject = ['authService', '$rootScope'];

    function authController(authService, $rootScope) {
      var vm = this;

      vm.emailFormat = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
      vm.passwordPattern = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/;
      vm.register = register;
      vm.userLogin = userLogin;

      function register(isValid){
        if(isValid){
          authService.register(vm.registration);
        }
      }

      function userLogin(isValid){
        if(isValid){
          authService.login(vm.login);
        }
      }
    }

})();
