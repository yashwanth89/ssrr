(function(){
  'use strict';

  angular
    .module('app')
    .service('authService', authService);

  authService.$inject = ['$http', '$window', '$state'];

  function authService($http, $window, $state){
    var o = {
      saveToken : saveToken,
      getToken : getToken,
      logout : logout,
      isLoggedIn : isLoggedIn,
      currentUser : currentUser,
      register : register,
      login : login
    }

    return o;

    function saveToken(token){
      $window.localStorage['SSRR-User-token'] = token;
    }

    function getToken(){
      return $window.localStorage['SSRR-User-token'];
    }

    function logout() {
      $window.localStorage.removeItem('SSRR-User-token');
      $state.go('login');
    }

    function isLoggedIn(){
      var token = o.getToken();
      var payload;

      if(token){
        payload = token.split('.')[1];
        payload = $window.atob(payload);
        payload = JSON.parse(payload);

        return payload.exp > Date.now() / 1000;
      } else {
        return false;
      }
    }

    function currentUser(){
      if(isLoggedIn()){
        var token = o.getToken();
        var payload = token.split('.')[1];
        payload = $window.atob(payload);
        payload = JSON.parse(payload);
        return {
          email : payload.email,
          name : payload.name
        };
      }
    }

    function register(registry){
      return $http.post('/users/register', registry)
        .then(registerComplete)
        .catch(registerFailed);

      function registerComplete(response){
        // o.saveToken(response.data.token);
        $state.go('home');
      }

      function registerFailed(error) {
        console.log(error);
      }
    }

    function login(user){
      return $http.post('/users/login', user)
        .then(loginComplete)
        .catch(loginFailed);

      function loginComplete(response){
        o.saveToken(response.data.token);
        $state.go('profile');
      }

      function loginFailed(error){
        console.log(error);
      }
    }

  }

})();
