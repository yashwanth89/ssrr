(function(){
  'use strict';

  angular
    .module('app')
    .controller('teamController', teamController)
    .controller('memberController', memberController);

    teamController.$inject = ['teamService', 'adminAuthService', '$state'];
    memberController.$inject = ['teamService'];

    function teamController(teamService, adminAuthService, $state){
      var vm = this;

      vm.team = teamService.team;
      vm.removeMember = removeMember;
      vm.memberPage = memberPage;
      vm.wlMembers = teamService.wlMembers;
      vm.authorizedMember = authorizedMember;
      vm.unAuthorizedMember = unAuthorizedMember;
      vm.setHierarcy = setHierarcy;

      vm.isAdminLoggedIn = adminAuthService.isLoggedIn;

      function removeMember(id, type, index){
        teamService.deleteMember(id, type, index);
      }

      function memberPage(id){
        $state.go('member', {id: id});
      }

      function authorizedMember(id, index){
        teamService.memberAccepted(id, index);
      }

      function unAuthorizedMember(id, index){
        teamService.memberDenied(id, index);
      }

      function setHierarcy(id, value){
        teamService.setHierarcy(id, value);
      }
    }

    function memberController(teamService){
      var vm = this;

      vm.member = teamService.member;
    }
})();
