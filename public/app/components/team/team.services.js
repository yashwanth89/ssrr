(function(){
  'use strict';

  angular
    .module('app')
    .factory('teamService', teamService);

  teamService.$inject = ['$http', 'adminAuthService'];

  function teamService($http, adminAuthService){
    var teamList = [];
    var teamObj = {};
    var team = {
      professorInCharge: [],
      associatedProfessors: [],
      researchers: [],
      pastMembers: []
    };
    var wlMembers = [];
    var member = {};
    var o = {
      teamList: teamList,
      teamObj: teamObj,
      team: team,
      member: member,
      getAll: getAll,
      getMember: getMember,
      getMemberPublications: getMemberPublications,
      getMemberProjects: getMemberProjects,
      deleteMember: deleteMember,
      wlMembers: wlMembers,
      getAllWl: getAllWl,
      memberAccepted: memberAccepted,
      memberDenied: memberDenied,
      setHierarcy: setHierarcy
    }

    return o;

    function getAll(){
      return $http.get('/team')
        .then(getComplete)
        .catch(getFailed);

      function getComplete(response){
        o.team = {
          professorInCharge: [],
          associatedProfessors: [],
          researchers: [],
          pastMembers: []
        }
        angular.copy(response.data, o.teamList);
        angular.forEach(o.teamList, function(value){
          o.teamObj[value._id] = value;
          if(value.profession === 'Professor-In-Charge')
            o.team.professorInCharge.push(value);
          if(value.profession === 'Associated Professor')
            o.team.associatedProfessors.push(value);
          if(value.profession === 'Researcher')
            o.team.researchers.push(value);
          if(value.profession === 'Past Member')
            o.team.pastMembers.push(value);
        });
      }

      function getFailed(error){

      }
    }

    function deleteMember(id, type, index){
      return $http.delete('/team/' + id, {
          data: { _id: id },
          headers: { Authorization: 'Bearer ' + adminAuthService.getToken()}
        })
        .then(deleteComplete)
        .catch(deleteFailed);

      function deleteComplete(response){
        if(type === 'Professor-In-Charge')
          o.team.professorInCharge.splice(index, 1);
        if(type === 'Associated Professor')
          o.team.associatedProfessors.splice(index, 1);
        if(type === 'Researcher')
          o.team.researchers.splice(index, 1);
        if(type === 'Past Member')
          o.team.pastMembers.splice(index, 1);
      }

      function deleteFailed(error){

      }
    }

    function getMember(id){
      if(o.teamList.length === 0){
        o.getAll()
          .then(function(res){
            angular.copy(o.teamObj[id], o.member);
          });
      } else{
        angular.copy(o.teamObj[id], o.member);
      }

      o.getMemberPublications(id);
      o.getMemberProjects(id);
    }

    function getMemberPublications(id){
      return $http.get('/team/publications/' + id)
        .then(getComplete)
        .catch(getFailed);

      function getComplete(response){
        o.member.publications = {
          bookChapters: [],
          journalPapers: [],
          conferencePapers: []
        };
        for(var i = 0; i < response.data.length; i++){
          if(response.data[i].type === 'incollection')
            o.member.publications.bookChapters.push(response.data[i]);
          else if(response.data[i].type === 'article')
            o.member.publications.journalPapers.push(response.data[i]);
          else if(response.data[i].type === 'inproceedings')
            o.member.publications.conferencePapers.push(response.data[i]);
        }
      }

      function getFailed(error){

      }
    }

    function getMemberProjects(id){
      return $http.get('/team/projects/' + id)
        .then(getComplete)
        .catch(getFailed);

      function getComplete(response){
        o.member.projects = response.data;
      }

      function getFailed(error){

      }
    }

    function getAllWl(){
      return $http.get('/users/newMembers')
        .then(getComplete)
        .catch(getFailed);

      function getComplete(response){
        angular.copy(response.data, o.wlMembers);
      }

      function getFailed(error){

      }
    }

    function memberAccepted(id, index){
      return $http.put('/users/acceptMeber/' + id, null, {headers: { Authorization: 'Bearer ' + adminAuthService.getToken()}})
        .then(acceptComplete)
        .catch(acceptFailed);

      function acceptComplete(response){
        o.wlMembers.splice(index, 1);
      }

      function acceptFailed(error){

      }
    }

    function memberDenied(id, index){
      return $http.delete('/users/denyMeber/' + id, {
          data: {_id: id},
          headers: { Authorization: 'Bearer ' + adminAuthService.getToken()}
        })
        .then(denyComplete)
        .catch(denyFailed);

      function denyComplete(response){
        o.wlMembers.splice(index, 1);
      }

      function denyFailed(error){

      }
    }

    function setHierarcy(id, value){
      return $http.put('/team/' + id + '/hierarchy/' + value, null, {headers: { Authorization: 'Bearer ' + adminAuthService.getToken()}})
        .then(setComplete)
        .catch(setFailed);

      function setComplete(response){
        if(response.data.profession === 'Professor-In-Charge')
          o.team.professorInCharge.hierarchy = response.data.hierarchy;
        if(response.data.profession === 'Associated Professor')
          o.team.associatedProfessors.hierarchy = response.data.hierarchy;
        if(response.data.profession === 'Researcher')
          o.team.researchers.hierarchy = response.data.hierarchy;
        if(response.data.profession === 'Past Member')
          o.team.pastMembers.hierarchy = response.data.hierarchy;
      }

      function setFailed(error) {

      }
    }

  }
})();
