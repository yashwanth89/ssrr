(function(){
  'use strict';

  angular
    .module('app')
    .controller('publicationsController', publicationsController);

  publicationsController.$inject = ['publicationsService'];

  function publicationsController(publicationsService){
    var vm = this;

    vm.publicationsObj = publicationsService.publicationsObj;
    vm.teamList = publicationsService.teamMembersList;
    vm.projectsList = publicationsService.projectsList;

    /* Bib content */
    vm.bibContentType = 'Add +';
    vm.bibContentArray = [];
    vm.bibContentIndex = null;
    vm.addBib = addBib;
    vm.removeBibContent = removeBibContent;
    vm.updateBibContent = updateBibContent;

    vm.addBibContent = addBibContent;

    /* Publication Form */
    vm.publicationType = 'Save';
    vm.publicationIndex = null;
    vm.addPublication = addPublication;
    vm.removePublication = removePublication;
    vm.updatePublication = updatePublication;

    // Authours
    vm.publication = {};
    vm.publication.author_short = [];
    vm.authourType = 'Add +';
    vm.addAuthor = addAuthor;
    vm.authourIndex = null;
    vm.removeAuthor = removeAuthor;
    vm.updateAuthor = updateAuthor;

    // Authours
    vm.publication.editor_short = [];
    vm.editorType = 'Add +';
    vm.addEditor = addEditor;
    vm.editorIndex = null;
    vm.removeEditor = removeEditor;
    vm.updateEditor = updateEditor;

    /* Bib content */
    function addBib() {
      if(vm.bibContentType === 'Update'){
        vm.bibContentArray[vm.bibContentIndex] = JSON.parse(JSON.stringify(eval("(" + vm.bibContent + ")")));
        vm.bibContentType = 'Add +';
      } else{
        vm.bibContentArray = JSON.parse(JSON.stringify(eval("(" + vm.bibContent + ")"))).data;
      }

      vm.bibContent = '';
    }

    function removeBibContent(index){
      vm.bibContentArray.splice(index, 1);
    }

    function updateBibContent(content, index){
      vm.bibContent = JSON.stringify(content);
      vm.bibContentType = 'Update';
      vm.bibContentIndex = index;
    }

    function addBibContent(){
      publicationsService.addBibContent(vm.bibContentArray);
      vm.bibContentArray = [];
    }

    /* Publication form */
    function addPublication(){
      if(vm.publicationType === 'Update'){
        publicationsService.updatePublication(vm.publication, vm.publicationIndex);
        vm.publicationType = 'Save';
      } else
        publicationsService.addPublication(vm.publication);

      vm.publication = {};
    }

    function removePublication(id, type, index) {
      publicationsService.deletePublication(id, type, index);
    }

    function updatePublication(publication, index) {
      vm.publication = publication;
      vm.publicationType = 'Update';
      vm.publicationIndex = index;
    }

    // Authors
    function addAuthor(){
      if(vm.authourType === 'Update'){
        vm.publication.author_short[vm.authourIndex] = vm.publication.authorName;
        vm.authourType = 'Add +';
      } else
        vm.publication.author_short.push(vm.publication.authorName);

      vm.publication.authorName = '';
    }

    function removeAuthor(index){
      vm.publication.author_short.splice(index, 1);
    }

    function updateAuthor(author, index){
      vm.publication.authorName = author;
      vm.authourType = 'Update';
      vm.authourIndex = index;
    }

    // Editors
    function addEditor(){
      if(vm.editorType === 'Update'){
        vm.publication.editor_short[vm.editorIndex] = vm.publication.editorName;
        vm.editorType = 'Add +';
      } else
        vm.publication.editor_short.push(vm.publication.editorName);

      vm.publication.editorName = '';
    }

    function removeEditor(index){
      vm.publication.editor_short.splice(index, 1);
    }

    function updateEditor(author, index){
      vm.publication.editorName = author;
      vm.editorType = 'Update';
      vm.editorIndex = index;
    }

  }
})();
