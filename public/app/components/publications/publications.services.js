(function(){
  'use strict';

  angular
    .module('app')
    .factory('publicationsService', publicationsService);

  publicationsService.$inject = ['$http', 'adminAuthService'];

  function publicationsService($http, adminAuthService) {
    var publicationsObj = {
      bookChapters: [],
      journalPapers: [],
      conferencePapers: []
    }
    var teamMembersList = [];
    var projectsList = [];
    var o = {
      publicationsObj: publicationsObj,
      getAll: getAll,
      addBibContent: addBibContent,
      addPublication: addPublication,
      deletePublication: deletePublication,
      updatePublication: updatePublication,
      teamMembersList: teamMembersList,
      getTeamMembers: getTeamMembers,
      projectsList: projectsList,
      getProjectsList: getProjectsList
    }

    return o;

    function getAll(){
      return $http.get('/publications')
        .then(getComplete)
        .catch(getFailed);

      function getComplete(response){
        if(!o.publicationsObj.bookChapters[0] || !o.publicationsObj.journalPapers[0] || !o.publicationsObj.conferencePapers[0]){
          for(var i = 0; i < response.data.length; i++){
            if(response.data[i].type === 'incollection')
    					o.publicationsObj.bookChapters.push(response.data[i]);
    				else if(response.data[i].type === 'article')
    					o.publicationsObj.journalPapers.push(response.data[i]);
    				else if(response.data[i].type === 'inproceedings')
    					o.publicationsObj.conferencePapers.push(response.data[i]);
          }
        }

      }

      function getFailed(error){

      }
    }

    function addBibContent(array){
      return $http.post('/publications/bibtext', array, {headers: {Authorization: 'Bearer ' + adminAuthService.getToken()}})
        .then(addComplete)
        .catch(addFailed);

      function addComplete(response){
        console.log(response);
      }

      function addFailed(error){

      }
    }

    function addPublication(publication){
      return $http.post('/publications', publication, {headers: {Authorization: 'Bearer ' + adminAuthService.getToken()}})
        .then(addComplete)
        .catch(addFailed);

      function addComplete(response){
        if(response.data.type === 'incollection')
          o.publicationsObj.bookChapters.push(response.data);
        else if(response.data.type === 'article')
          o.publicationsObj.journalPapers.push(response.data);
        else if(response.data.type === 'inproceedings')
          o.publicationsObj.conferencePapers.push(response.data);
      }

      function addFailed(error){

      }
    }

    function deletePublication(id, type, index) {
      return $http.delete('/publications/' + id, {
        data: {_id: id},
        headers: {Authorization: 'Bearer ' + adminAuthService.getToken()}
      })
        .then(deleteComplete)
        .catch(deleteFailed);

      function deleteComplete(response){
        console.log(response.data);
        o.publicationsObj[type].splice(index,1);
      }

      function deleteFailed(error){

      }
    }

    function updatePublication(publication, index) {
      return $http.put('/publications/' + publication._id, publication, {headers: {Authorization: 'Bearer ' + adminAuthService.getToken()}})
        .then(updateComplete)
        .catch(updateFailed);

      function updateComplete(response){
        if(response.data.type === 'incollection')
          o.publicationsObj.bookChapters[index] = response.data;
        else if(response.data.type === 'article')
          o.publicationsObj.journalPapers[index] = response.data;
        else if(response.data.type === 'inproceedings')
          o.publicationsObj.conferencePapers[index] = response.data;

        o.publicationsObj.unedited.splice(index, 1);
      }

      function updateFailed(error) {

      }
    }

    function getTeamMembers() {
      return $http.get('/publications/teamMembers')
        .then(getComplete)
        .catch(getFailed);

      function getComplete(response){
        angular.copy(response.data, o.teamMembersList);
      }

      function getFailed(error){

      }
    }

    function getProjectsList(){
      return $http.get('/publications/projectsList')
        .then(getComplete)
        .catch(getFailed);

      function getComplete(response){
        angular.copy(response.data, o.projectsList);
      }

      function getFailed(error){

      }
    }

  }
})();
