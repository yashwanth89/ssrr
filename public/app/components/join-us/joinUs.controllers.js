(function(){
  'use strict';

  angular
    .module('app')
    .controller('joinUsController', joinUsController);

  joinUsController.$inject = ['joinUsService', 'adminAuthService'];

  function joinUsController(joinUsService, adminAuthService){
    var vm = this;

    vm.contentObj = joinUsService.contentObj;
    vm.type = 'Save';
    vm.addOrSaveJoinUs = addOrSaveJoinUs;
    vm.updateJoinUs = updateJoinUs;

    vm.isAdminLoggedIn = adminAuthService.isLoggedIn;

    function addOrSaveJoinUs() {
      if(vm.type == 'Update'){
        joinUsService.update(vm.newContentObj);
        vm.type = 'Save';
      } else
         joinUsService.create(vm.newContentObj, vm.contentObj._id);

      vm.newContentObj = {};
    }

    function updateJoinUs(contentObj){
      vm.type = 'Update';
      vm.newContentObj = contentObj;
    }
  }

})();
