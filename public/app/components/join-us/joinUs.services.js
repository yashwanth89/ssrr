(function(){
  'use strict';

  angular
    .module('app')
    .factory('joinUsService', joinUsService);

  joinUsService.$inject = ['$http', 'adminAuthService'];

  function joinUsService($http, adminAuthService){
    var contentObj = {};
    var o = {
      contentObj: contentObj,
      getContent: getContent,
      create: create,
      update: update
    }

    return o;

    function getContent(){
      return $http.get('/joinUs')
        .then(getComplete)
        .catch(getFailed);

      function getComplete(response) {
        angular.copy(response.data, o.contentObj);
      }

      function getFailed(error){

      }

    }

    function create(contentObj, id){
      return $http.post('/joinUs/' + id, contentObj, {headers: { Authorization: 'Bearer ' + adminAuthService.getToken()}})
        .then(createComplete)
        .catch(createFailed);

      function createComplete(response){
        console.log(response);
        angular.copy(response.data, o.contentObj);
      }

      function createFailed(error){

      }
    }

    function update(contentObj){
      return $http.put('/joinUs/' + contentObj._id, contentObj, {headers: { Authorization: 'Bearer ' + adminAuthService.getToken()}})
        .then(updateComplete)
        .catch(updateFailed);

      function updateComplete(response) {
        angular.copy(response.data, o.contentObj);
      }

      function updateFailed(error){

      }
    }

  }

})();
