(function() {
  'use strict';

  angular
    .module('app')
    .controller('AbstractController', AbstractController)
    .controller('CarouselController', CarouselController)
    .controller('ResearchController', ResearchController)
    .controller('ContactUsController', ContactUsController);

  AbstractController.$inject = ['aboutUsService', 'adminAuthService'];
  CarouselController.$inject = ['carouselService', 'adminAuthService'];
  ResearchController.$inject = ['researchService', 'adminAuthService'];
  ContactUsController.$inject = ['contactUsService', 'adminAuthService'];

  function AbstractController(aboutUsService, adminAuthService){
    var vm = this;

    vm.contentObj = aboutUsService.contentObj;
    vm.type = 'Save';
    vm.addOrSaveAboutUs = addOrSaveAboutUs;
    vm.updateAboutUs = updateAboutUs;

    vm.isAdminLoggedIn = adminAuthService.isLoggedIn;

    function addOrSaveAboutUs(){
      if(vm.type == 'Update'){
        aboutUsService.update(vm.newContentObj);
        vm.type = 'Save';
      } else
        aboutUsService.create(vm.newContentObj);

      vm.newContentObj = {};
    }

    function updateAboutUs(contentObj){
      vm.type = 'Update';
      vm.newContentObj = contentObj;
    }
  }

  function CarouselController(carouselService, adminAuthService){
    var vm = this;

    vm.myInterval = 5000;
    vm.noWrapSlides = false;
    vm.active = 0;

    vm.type = 'Save';
    vm.carouselIndex = null;
    vm.slides = carouselService.carousel;
    vm.addOrSaveSlide = addOrSaveSlide;
    vm.updateSlide = updateSlide;
    vm.removeSlide = removeSlide;

    vm.isAdminLoggedIn = adminAuthService.isLoggedIn;

    function addOrSaveSlide(){
      if(vm.type == 'Update'){
        carouselService.update(vm.carouselObj, vm.carouselIndex);
        vm.type = 'Save';
      } else
        carouselService.create(vm.carouselObj);

      vm.carouselObj = {};
    }

    function updateSlide(slide, index){
      vm.carouselObj = slide;
      vm.carouselIndex = index;
      vm.type = 'Update';
    }

    function removeSlide(id, index){
      carouselService.deleteSlide(id, index);
    }
  }

  function ResearchController(researchService, adminAuthService) {
    var vm = this;
    vm.areas = researchService.researchList;

    vm.type = 'Save';
    vm.areaIndex = null;
    vm.addOrSaveArea = addOrSaveArea;
    vm.updateArea = updateArea;
    vm.removeArea = removeArea;

    vm.isAdminLoggedIn = adminAuthService.isLoggedIn;

    function addOrSaveArea(){
      if(vm.type == 'Update'){
        researchService.update(vm.newArea, vm.areaIndex);
        vm.type = 'Save'
      } else
        researchService.create(vm.newArea);

      vm.newArea = {};
    }

    function updateArea(area, index){
      vm.newArea = area;
      vm.areaIndex = index;
      vm.type = 'Update';
    }

    function removeArea(id, index){
      researchService.deleteArea(id, index);
    }

  }

  function ContactUsController(contactUsService, adminAuthService){
    var vm = this;

    vm.contactUs = contactUsService.contacts;

    vm.type = 'Save';
    vm.addOrSaveContactUs = addOrSaveContactUs;
    vm.updateContactUs = updateContactUs;

    vm.isAdminLoggedIn = adminAuthService.isLoggedIn;

    function addOrSaveContactUs(){
      if(vm.type == 'Update'){
        contactUsService.update(vm.contact, vm.contactUs._id);
        vm.type = 'Save';
      } else
        contactUsService.create(vm.contact);

      vm.contact = {};
    }

    function updateContactUs(contacts){
      vm.type = 'Update';
      vm.contact = contacts;
    }
  }

})();
