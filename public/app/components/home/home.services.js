(function() {
  'use strict';

  angular
    .module('app')
    .factory('aboutUsService', aboutUsService)
    .factory('carouselService', carouselService)
    .factory('researchService', researchService)
    .factory('contactUsService', contactUsService);

  aboutUsService.$inject = ['$http', 'adminAuthService'];
  carouselService.$inject = ['$http', 'Upload', 'adminAuthService'];
  researchService.$inject = ['$http', 'adminAuthService'];
  contactUsService.$inject = ['$http', 'adminAuthService'];

  function aboutUsService($http, adminAuthService){
    var contentObj = {};
    var o = {
      contentObj: contentObj,
      getContent: getContent,
      create: create,
      update : update
    }

    return o;

    function getContent(){
      return $http.get('/home/aboutUs')
        .then(getComplete)
        .catch(getFailed);

      function getComplete(response){
        o.contentObj = response.data[0];
      }

      function getFailed(error){

      }
    }

    function create(contentObj){
      return $http.post('/home/aboutUs', contentObj, {headers: { Authorization: 'Bearer ' + adminAuthService.getToken()}})
        .then(createComplete)
        .catch(createFailed);

      function createComplete(response){
        o.contentObj = response.data[0];
      }

      function createFailed(error){

      }
    }

    function update(contentObj){
      return $http.put('/home/aboutUs/' + contentObj._id, contentObj, {headers: { Authorization: 'Bearer ' + adminAuthService.getToken()}})
        .then(updateComplete)
        .catch(updateFailed);

      function updateComplete(response){
        o.contentObj = response.data[0];
      }

      function updateFailed(error){

      }
    }

  }

  function carouselService($http, Upload, adminAuthService){
    var carousel = [];
    var o = {
      carousel : carousel,
      getAll : getAll,
      create : create,
      update : update,
      deleteSlide : deleteSlide
    }

    return o;

    function getAll(){
      return $http.get('/home/carousel')
        .then(getComplete)
        .catch(getFailed);

      function getComplete(response){
        angular.copy(response.data, o.carousel);
      }

      function getFailed(error) {

      }
    }

    function create(slide){
      return Upload.upload({ // if using progresss use Upload else use $http.put
          url: '/home/carousel',
          method: 'POST',
          data: { captionTitle: slide.captionTitle, captionText: slide.captionText, image: slide.image },
          headers: { Authorization: 'Bearer ' + adminAuthService.getToken()}
        })
        .progress(createProgress)
        .then(createComplete)
        .catch(createFailed);

      function createProgress(event){
        console.log(Math.floor((event.loaded / event.total)*100)+ "%");
      }

      function createComplete(response){
        o.carousel.push(response.data);
      }

      function createFailed(error){

      }
    }

    function update(slide, index){
      return Upload.upload({ // if using progresss use Upload else use $http.put
          url: '/home/carousel/' + slide._id,
          method: 'PUT',
          data: {captionTitle: slide.captionTitle, captionText: slide.captionText, image: slide.image, imageUrl: slide.imageUrl},
          headers: { Authorization: 'Bearer ' + adminAuthService.getToken()}
        })
        .progress(createProgress)
        .then(createComplete)
        .catch(createFailed);

      function createProgress(event){
        console.log(Math.floor((event.loaded / event.total)*100)+ "%");
      }

      function createComplete(response){
        o.carousel[index] = response.data;
      }

      function createFailed(error){

      }
    }

    function deleteSlide(id, index){
      return $http.delete('/home/carousel/' + id, {
          data: { _id: id },
          headers: { Authorization: 'Bearer ' + adminAuthService.getToken()}
        })
        .then(deleteComplete)
        .catch(deleteFailed);

      function deleteComplete(response){
        o.carousel.splice(index, 1);
      }

      function deleteFailed(error){

      }
    }
  }

  function researchService($http, adminAuthService){
    var researchList = [];
    var o = {
      researchList : researchList,
      getAll : getAll,
      create : create,
      update : update,
      deleteArea : deleteArea
    }

    return o;

    function getAll(){
      return $http.get('/home/research')
        .then(getComplete)
        .catch(getFailed);

      function getComplete(response){
        angular.copy(response.data, o.researchList);
      }

      function getFailed(error){

      }
    }

    function create(area){
      return $http.post('/home/research', area, {headers: { Authorization: 'Bearer ' + adminAuthService.getToken()}})
        .then(createComplete)
        .catch(createFailed);

      function createComplete(response){
        o.researchList.push(response.data);
      }

      function createFailed(error){

      }
    }

    function update(area, index){
      return $http.put('/home/research/' + area._id, area, {headers: { Authorization: 'Bearer ' + adminAuthService.getToken()}})
        .then(updateComplete)
        .catch(updateFailed);

      function updateComplete(response){
        o.researchList[index] = response.data;
      }

      function updateFailed(error){

      }
    }

    function deleteArea(id, index){

      return $http.delete('/home/research/' + id, {
          data: { _id: id },
          headers: { Authorization: 'Bearer ' + adminAuthService.getToken()}
        })
        .then(deleteComplete)
        .catch(deleteFailed);

      function deleteComplete(response){
        o.researchList.splice(index, 1);
      }

      function deleteFailed(error){

      }
    }
  }

  function contactUsService($http, adminAuthService){
    var contacts = {};
    var o = {
      contacts: contacts,
      getcontacts: getcontacts,
      create: create,
      update: update
    }

    return o;

    function getcontacts(){
      return $http.get('/home/contactUs')
        .then(getComplete)
        .catch(getFailed);

      function getComplete(response){
        angular.copy(response.data, o.contacts);
      }

      function getFailed(error){

      }
    }

    function create(contacts){
      return $http.post('/home/contactUs', contacts, {headers: { Authorization: 'Bearer ' + adminAuthService.getToken()}})
        .then(createComplet)
        .catch(createFailed);

      function createComplet(response){
        angular.copy(response.data, o.contacts);
      }

      function createFailed(error){

      }
    }

    function update(contacts){
      return $http.put('/home/contactUs/' + contacts._id, contacts, {headers: { Authorization: 'Bearer ' + adminAuthService.getToken()}})
        .then(updateComplete)
        .catch(updateFailed);

      function updateComplete(response){
        angular.copy(response.data, o.contacts);
      }

      function updateFailed(error){

      }
    }
  }

})();
