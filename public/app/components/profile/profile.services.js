(function(){
  'use strict';

  angular
    .module('app')
    .service('profileService', profileService);

  profileService.$inject = ['$http', 'authService', 'Upload', 'teamService'];

  function profileService($http, authService, Upload, teamService){
    var user = {};
    var projectsList = [];
    var projectsObj = {};
    var o = {
      user : user,
      getProfile : getProfile,
      createCV : createCV,
      createDP : createDP,
      updateProfile : updateProfile,
      getUserPublications: getUserPublications,
      getUserProjects: getUserProjects,
      projectsList: projectsList,
      projectsObj: projectsObj
    }

    return o;

    function getProfile(){
      return $http.get('/users/profile', {
        headers: {
          Authorization: 'Bearer '+ authService.getToken()
        }})
        .then(getProfileCompelete)
        .catch(getProfileFailed);

      function getProfileCompelete(response){
        angular.copy(response.data, o.user);

        getUserPublications(o.user._id);
        getUserProjects(o.user._id);

        // Add in the team service
        if(!teamService.teamObj[o.user._id]){
          teamService.teamObj[o.user._id] = o.user;
          if(o.user.profession === 'Professor-In-Charge')
            teamService.team.professorInCharge.push(o.user);
          if(o.user.profession === 'Associated Professor')
            teamService.team.associatedProfessors.push(o.user);
          if(o.user.profession === 'Researcher')
            teamService.team.researchers.push(o.user);
          if(o.user.profession === 'Past Member')
            teamService.team.pastMembers.push(o.user);
        }

      }

      function getProfileFailed(error){

      }
    }

    function createCV(id, file){
      return Upload.upload({
          url: '/profile/CV/' + id,
          method: 'PUT',
          data: {CV: file},
          headers: {
            Authorization: 'Bearer ' + authService.getToken()
          }
        })
        .progress(createCvProgress)
        .then(createCvComplete)
        .catch(createCvFailed);

      function createCvProgress(event){
        console.log(Math.floor((event.loaded / event.total)*100)+ "%");
      }

      function createCvComplete(response){
        o.user["cvUrl"] = response.data.cvUrl;
      }

      function createCvFailed(error){

      }
    }

    function createDP(id, file){
      return Upload.upload({
          url: '/profile/DP/' + id,
          method: 'PUT',
          data: {DP: file},
          headers: {
            Authorization: 'Bearer ' + authService.getToken()
          }
        })
        .progress(createDpProgress)
        .then(createDpComplete)
        .catch(createDpFailed);

      function createDpProgress(event){
        console.log(Math.floor((event.loaded / event.total)*100)+ "%");
      }

      function createDpComplete(response){
        o.user["dpUrl"] = response.data.dpUrl;
      }

      function createDpFailed(error){

      }
    }

    function updateProfile(id, user){
      // console.log(user);
      return $http.put('/profile/' + id, user, {headers: {Authorization: 'Bearer ' + authService.getToken()}})
        .then(updateComplete)
        .catch(updateFailed);

      function updateComplete(response){
        o.user = response.data;
      }

      function updateFailed(error){

      }
    }

    function getUserPublications(id){
      return $http.get('/profile/publications/' + id)
        .then(getComplete)
        .catch(getFailed);

      function getComplete(response){
        o.user.publications = {
          bookChapters: [],
          journalPapers: [],
          conferencePapers: []
        };
        for(var i = 0; i < response.data.length; i++){
          if(response.data[i].type === 'Book chapters')
            o.user.publications.bookChapters.push(response.data[i]);
          else if(response.data[i].type === 'Journal papers')
            o.user.publications.journalPapers.push(response.data[i]);
          else if(response.data[i].type === 'Conference papers')
            o.user.publications.conferencePapers.push(response.data[i]);
        }
      }

      function getFailed(error){

      }
    }

    function getUserProjects(id){
      return $http.get('/profile/projects/' + id)
        .then(getComplete)
        .catch(getFailed);

      function getComplete(response){
        o.user.projects = response.data;
      }

      function getFailed(error){

      }
    }

  }
})();
