(function(){
  'use strict';

  angular
    .module('app')
    .controller('profileController', profileController);

    profileController.$inject = ['profileService'];

    function profileController(profileService){
      var vm = this;

      vm.user = profileService.user;
      vm.projectsList = profileService.projectsList;

      vm.uploadCV = uploadCV;
      vm.uploadDP = uploadDP;

      /* Contacts */
      vm.addContact = addContact;
      vm.contactType = 'Add +';
      vm.contactIndex = null;
      vm.removeContact = removeContact;
      vm.updateContact = updateContact;

      /* Memberships */
      vm.addMembership = addMembership;
      vm.membershipType = 'Add +';
      vm.membershipIndex = null;
      vm.removeMembership = removeMembership;
      vm.updateMembership = updateMembership;

      /* Social links */
      vm.addSocialLink = addSocialLink;
      vm.socialLinkType = 'Add +';
      vm.socialLinkIndex = null;
      vm.removeSocialLink = removeSocialLink;
      vm.updateSocialLink = updateSocialLink;
      // vm.hoverEnter = hoverEnter;
      // vm.hoverLeave = hoverLeave;

      /* Interests */
      vm.interestType = 'Add +';
      vm.interestIndex = null;
      vm.addInterests = addInterests;
      vm.removeInterest = removeInterest;
      vm.updateInterest = updateInterest;

      /* Publications */
      vm.addPublication = addPublication;

      /* Teching courses */

      vm.addTeachingCourse = addTeachingCourse;
      vm.teachingCourseType = 'Add +';
      vm.teachingCourseIndex = null;
      vm.removeTeachingCourse = removeTeachingCourse;
      vm.updateTeachingCourse = updateTeachingCourse;

      vm.updateProfile = updateProfile;

      function uploadCV(id, file){
        profileService.createCV(id, file);
      }

      function uploadDP(id, file){
        profileService.createDP(id, file);
      }

      /* Contacts */
      function addContact(){
        if(vm.contactType === 'Update'){
          vm.user.contacts[vm.contactIndex] = vm.user.contact;
          vm.contactType = 'Add +';
        } else {
          if(vm.user.contact.type == 'telephone')
            vm.user.contact.attr = 'tel';
          if(vm.user.contact.type == 'email')
            vm.user.contact.attr = 'mailto';
          vm.user.contacts.push(vm.user.contact);
        }


        vm.user.contact = {};
      }
      function removeContact(index){
        vm.user.contacts.splice(index, 1);
      }
      function updateContact(contact, index){
        vm.user.contact = contact;
        vm.contactType = 'Update';
        vm.contactIndex = index;
      }

      /* Memberships */
      function addMembership(){
        if(vm.membershipType === 'Update'){
          vm.user.memberships[vm.membershipIndex] = vm.user.membership;
          vm.membershipType = 'Add +';
        } else
          vm.user.memberships.push(vm.user.membership);

        vm.user.membership = {};
      }
      function removeMembership(index){
        vm.user.memberships.splice(index, 1);
      }
      function updateMembership(membership, index){
        vm.user.membership = membership;
        vm.membershipType = 'Update';
        vm.membershipIndex = index;
      }

      /* Social links */
      function addSocialLink(){
        if(vm.socialLinkType == 'Update'){
          vm.user.socialLinks[vm.socialLinkIndex] = vm.user.socialLink;
          vm.socialLinkType = 'Add +';
        } else
          vm.user.socialLinks.push(vm.user.socialLink);

        vm.user.socialLink = {};
      }
      function removeSocialLink(index){
        vm.user.socialLinks.splice(index, 1);
      }
      function updateSocialLink(socialLink, index){
        vm.user.socialLink = socialLink;
        vm.socialLinkType = 'Update';
        vm.socialLinkIndex = index;
      }
      // function hoverEnter(){
      //   vm.hover = '_hover'
      // }
      // function hoverLeave(){
      //   vm.hover = ''
      // }

      /* Interests */
      function addInterests(){
        if(vm.interestType === 'Update'){
          vm.user.researchInterests[vm.interestIndex] = vm.user.researchInterest;
          vm.interestType = 'Add +';
        } else
          vm.user.researchInterests.push(vm.user.researchInterest);

        vm.user.researchInterest = {};
      }
      function removeInterest(index){
        vm.user.researchInterests.splice(index, 1);
      }
      function updateInterest(area, index){
        vm.interestType = 'Update';
        vm.user.researchInterest = area;
        vm.interestIndex = index;
      }

      function addPublication(){ // TODO: validation for form
        if(vm.user.publication.type === 'Book chapters')
          vm.user.publications.bookChapters.push(vm.user.publication);
        if(vm.user.publication.type === 'Journal papers')
          vm.user.publications.journalPapers.push(vm.user.publication);
        if(vm.user.publication.type === 'Conference papers')
          vm.user.publications.conferencePapers.push(vm.user.publication);
        vm.user.publication = {};
      }

      /* Teching courses */
      function addTeachingCourse(){
        if(vm.teachingCourseType == 'Update'){
          vm.user.teachingCourses[vm.teachingCourseIndex] = vm.user.teachingCourse;
          vm.teachingCourseType = 'Add +';
        } else
          vm.user.teachingCourses.push(vm.user.teachingCourse);

        vm.user.teachingCourse = {};
      }
      function removeTeachingCourse(index){
        vm.user.teachingCourses.splice(index, 1);
      }
      function updateTeachingCourse(teachingCourse, index){
        vm.teachingCourseType = 'Update';
        vm.user.teachingCourse = teachingCourse;
        vm.teachingCourseIndex = index;
      }

      /* Update whole profile */
      function updateProfile(id){
        vm.user.projects = [];

        profileService.updateProfile(id, vm.user);
      }
    }
})();
