(function(){
  'use strict';

  angular
    .module('app')
    .controller('NavCtrl', NavCtrl)
    .controller('navController', navController);

  NavCtrl.$inject = ['adminAuthService', '$location', '$rootScope'];
  navController.$inject = ['authService', 'profileService'];

  function NavCtrl(adminAuthService, $location, $rootScope){
    var vm = this;

    vm.isAdminLoggedIn = adminAuthService.isLoggedIn;
    vm.currentAdmin = adminAuthService.currentAdmin;
    vm.adminLogout = adminAuthService.logOut;

    $rootScope.$on('$stateChangeSuccess', function(event) {
      $rootScope.currentState = $location.path().split('/')[1];
    });
  }

  function navController(authService, profileService){
    var vm = this;

    vm.isLoggedIn = authService.isLoggedIn;
    vm.currentUser = authService.currentUser;
    vm.userLogout = authService.logout;
    vm.user = profileService.user;

  }
})();
