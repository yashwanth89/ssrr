(function(){
  'use strict';

  angular
    .module('app')
    .directive('scrollEvents', scrollEvents);

  scrollEvents.$inject = ['$document', '$timeout'];

  function scrollEvents($document, $timeout){
    var directive = {
      link: link
    }

    return directive;

    function link(scope, element, attrs){

      $document.bind('scroll', function(){
        if($document.scrollTop() > 100)
          $('.fixed-menu, .to-the-top').removeClass('display-none').addClass('active');
        else
          $('.fixed-menu, .to-the-top').removeClass('active').addClass('display-none');
      });
    }
  }
})();
