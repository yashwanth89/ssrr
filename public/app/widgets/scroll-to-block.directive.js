(function(){
  'use strict';

  angular
    .module('app')
    .directive('scrollToBlock', scrollToBlock);

  scrollToBlock.$inject = [];

  function scrollToBlock(){
    var directive = {
      restrict: 'A',
      scope: {
        scrollToBlock: '='
      },
      link: link
    }

    return directive;

    function link(scope, element, attrs) {
      element.bind('click', function(){
        angular.element('html,body').animate({scrollTop: angular.element('#' + scope.scrollToBlock).offset().top - 100}, 200);
      });
    }
  }
})();
