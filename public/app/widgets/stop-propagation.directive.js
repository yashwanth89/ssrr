(function(){
  'use strict';

  angular
    .module('app')
    .directive('stopPropagation', stopPropagation);

  stopPropagation.$inject = [];

  function stopPropagation(){
    var directive = {
      link: link
    }

    return directive;

    function link(scope, element, attrs){
      element.on('click', function(e){
        e.stopPropagation();
      });
    }
  }
})();
