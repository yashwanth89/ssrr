(function(){
  'use strict';

  angular
    .module('app')
    .directive('preventDefault', preventDefault);

  preventDefault.$inject = [];

  function preventDefault(){
    var directive = {
      link: link
    }

    return directive;

    function link(scope, element, attrs){
      // if(attrs.ngClick){
        element.on('click', function(e){
          e.preventDefault();
        });
      // }
    }
  }
})();
