(function(){
  'use strict';

  angular
    .module('app')
    .directive('pwCheck', pwCheck);

  function pwCheck(){
    var directive = {
      require: 'ngModel',
      scope: {
        password: '=pwCheck'
      },
      link: link
    }

    return directive;

    function link(scope, element, attrs, ngModel){
      ngModel.$validators.pwCheck = function(modelValue) {
        return modelValue == scope.password;
      };

      scope.$watch("password", function() {
        ngModel.$validate();
      });
    }
  }
})();
