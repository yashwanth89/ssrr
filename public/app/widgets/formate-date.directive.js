(function(){
  'use strict';

  angular
    .module('app')
    .directive('formateDate', formateDate);

  formateDate.$inject = ['dateFilter'];

  function formateDate(dateFilter) {
    var directive = {
      require: 'ngModel',
      scope: {
        format: "="
      },
      link: link
    }

    return directive;

    function link(scope, element, attrs, ngModelController) {
      ngModelController.$parsers.push(function(data) {
        //convert data from view format to model format
        return dateFilter(data, scope.format); //converted
      });

      ngModelController.$formatters.push(function(data) {
        //convert data from model format to view format
        return dateFilter(data, scope.format); //converted
      });
    }
  }
})();
